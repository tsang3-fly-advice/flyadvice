package net.suncaper.flightanalysis.domain;

public class MonthPredict {

    int month;
    int min_price;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMin_price() {
        return min_price;
    }

    public void setMin_price(int min_price) {
        this.min_price = min_price;
    }

}
