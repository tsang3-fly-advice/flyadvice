package net.suncaper.flightanalysis.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import net.suncaper.flightanalysis.domain.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.swing.plaf.basic.BasicListUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class FlightListController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void getFlights(List<Flight> flights, List<Flight> flightsCat, JSONArray route_list)
    {
        for(int k = 0; k < route_list.size(); k++)
        {
            JSONArray route_list_sub = route_list.getJSONArray(k);
            for(int i = 0; i < route_list_sub.size(); i++)
            {
                JSONObject flightInfo = route_list_sub.getJSONObject(i);
                boolean isCat = flightInfo.getBoolean("auto_cat");
                JSONArray flightsData = flightInfo.getJSONArray("legs");
                for(int j = 0; j < flightsData.size(); j++)
                {
                    JSONObject flightObject = flightsData.getJSONObject(j);
                    System.out.println(flightObject);
                    Flight flight = JSON.toJavaObject(flightObject, Flight.class);
                    if(isCat)
                    {
                        flightsCat.add(flight);
                    }
                    else
                    {
                        flights.add(flight);
                    }
                }
            }
        }
    }

    public void makeJSONOneWay(JSONObject jsonObject, String departureAirport, String arrivalAirport, String departureDate)
    {
        List<String> departure_airport = new ArrayList<String>();
        departure_airport.add(departureAirport);
        List<String> arrival_airport = new ArrayList<String>();
        arrival_airport.add(arrivalAirport);
        List<String> date = new ArrayList<String>();
        date.add(departureDate);

        jsonObject.put("departure_airport", departure_airport);
        jsonObject.put("arrival_airport", arrival_airport);
        jsonObject.put("date", date);
        jsonObject.put("type", "OneWay");
    }

    @RequestMapping("/searchOneWay")
    public String SearchOneWay(Model model, String departureAirport, String arrivalAirport, String departureDate)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        headers.setContentType(type);

        JSONObject postJSON = new JSONObject();
        makeJSONOneWay(postJSON, departureAirport, arrivalAirport, departureDate);
        String jsonString = restTemplate.postForObject("http://10.8.0.10:8000/test/", postJSON, String.class);

        JSONObject jsonData = JSONObject.parseObject(jsonString);
        JSONArray route_list = jsonData.getJSONArray("route_list");

        List<Flight> flights = new ArrayList<Flight>();
        List<Flight> flightsCat = new ArrayList<Flight>();

        getFlights(flights, flightsCat, route_list);

        model.addAttribute("flights", flights);
        model.addAttribute("flightsCat", flightsCat);

        return "FlightList";
    }

    public void makeJSONTwoWay(JSONObject jsonObject, String DAirport1, String AAirport1, String DDate1, String DAirport2, String AAirport2, String DDate2)
    {
        List<String> departure_airport = new ArrayList<String>();
        departure_airport.add(DAirport1);
        departure_airport.add(DAirport2);
        List<String> arrival_airport = new ArrayList<String>();
        arrival_airport.add(AAirport1);
        arrival_airport.add(AAirport2);
        List<String> date = new ArrayList<String>();
        date.add(DDate1);
        date.add(DDate2);

        jsonObject.put("departure_airport", departure_airport);
        jsonObject.put("arrival_airport", arrival_airport);
        jsonObject.put("date", date);
        jsonObject.put("type", "TwoWay");
    }

    @RequestMapping("/searchTwoWay")
    public String SearchTwoWay(Model model, String departureAirportOne, String arrivalAirportOne, String departureDateOne, String departureAirportTwo, String arrivalAirportTwo, String departureDateTwo)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        headers.setContentType(type);

        JSONObject postJSON = new JSONObject();
        makeJSONTwoWay(postJSON, departureAirportOne, arrivalAirportOne, departureDateOne, departureAirportTwo, arrivalAirportTwo, departureDateTwo);
        String jsonString = restTemplate.postForObject("http://10.8.0.10:8000/test/", postJSON, String.class);

        JSONObject jsonData = JSONObject.parseObject(jsonString);
        JSONArray route_list = jsonData.getJSONArray("route_list");

        List<Flight> flights = new ArrayList<Flight>();
        List<Flight> flightsCat = new ArrayList<Flight>();

        getFlights(flights, flightsCat, route_list);

        model.addAttribute("flights", flights);
        model.addAttribute("flightsCat", flightsCat);

        return "FlightList";
    }

    public void makeJSONRoundTrip(JSONObject jsonObject, String departureAirport, String arrivalAirport, String departureDateTo, String departureDateFrom)
    {
        List<String> departure_airport = new ArrayList<String>();
        departure_airport.add(departureAirport);
        departure_airport.add(arrivalAirport);
        List<String> arrival_airport = new ArrayList<String>();
        arrival_airport.add(arrivalAirport);
        arrival_airport.add(departureAirport);
        List<String> date = new ArrayList<String>();
        date.add(departureDateTo);
        date.add(departureDateFrom);

        jsonObject.put("departure_airport", departure_airport);
        jsonObject.put("arrival_airport", arrival_airport);
        jsonObject.put("date", date);
        jsonObject.put("type", "TwoWay");
    }

    @RequestMapping("/searchRoundTrip")
    public String SearchRoundTrip(Model model, String departureAirport, String arrivalAirport, String departureDateTo, String departureDateFrom)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        headers.setContentType(type);

        JSONObject postJSON = new JSONObject();
        makeJSONRoundTrip(postJSON, departureAirport, arrivalAirport, departureDateTo, departureDateFrom);
        String jsonString = restTemplate.postForObject("http://10.8.0.10:8000/test/", postJSON, String.class);

        JSONObject jsonData = JSONObject.parseObject(jsonString);
        JSONArray route_list = jsonData.getJSONArray("route_list");

        List<Flight> flights = new ArrayList<Flight>();
        List<Flight> flightsCat = new ArrayList<Flight>();

        getFlights(flights, flightsCat, route_list);

        model.addAttribute("flights", flights);
        model.addAttribute("flightsCat", flightsCat);

        return "FlightList";
    }

    @RequestMapping("/flightList")
    public String showWorldMap(Model model)
    {
        return "FlightList";
    }

}
