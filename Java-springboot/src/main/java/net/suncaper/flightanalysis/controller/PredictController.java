package net.suncaper.flightanalysis.controller;

import net.suncaper.flightanalysis.domain.DayPredict;
import net.suncaper.flightanalysis.domain.MonthPredict;
import org.omg.CORBA.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Controller
public class PredictController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @RequestMapping("/searchMonth")
    public String MonthPredict(Model model, String departureCity, String arrivalCity)
    {
        String sql = "select month(departure_time) month, min(price) min_price from flight " +
                "where departure_city = '"+ departureCity +"' and arrival_city = '" + arrivalCity + "' and month(departure_time) > month(now()) " +
                "group by month;";

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql);

        List<MonthPredict> monthPredicts = new ArrayList<MonthPredict>();

        Calendar calendar = Calendar.getInstance();
        int lastMonth = calendar.get(Calendar.MONTH) + 1;
        int currentMonth = 0;

        for(int i = 0; i < results.size(); i++)
        {
            MonthPredict monthPredict = new MonthPredict();
            Map<String, Object> map = results.get(i);
            for(String key : map.keySet())
            {
                if(key.equals("month"))
                {
                    currentMonth = (Integer) map.get(key);
                    monthPredict.setMonth(currentMonth);
                }
                else if(key.equals("min_price"))
                {
                    monthPredict.setMin_price((Integer) map.get(key));
                }
            }

            for(int j = 0; j < currentMonth - lastMonth - 1; j++)
            {
                MonthPredict monthValueOfZero = new MonthPredict();
                monthValueOfZero.setMonth(lastMonth + j + 1);
                monthValueOfZero.setMin_price(0);
                monthPredicts.add(monthValueOfZero);
            }
            lastMonth = currentMonth;
            monthPredicts.add(monthPredict);
        }

        model.addAttribute("monthPredicts", monthPredicts);
        model.addAttribute("departureCity", departureCity);
        model.addAttribute("arrivalCity", arrivalCity);

        return "Prediction";
    }

    @RequestMapping("/searchDay")
    @ResponseBody
    public List<DayPredict> DayPredict(String departureCity, String arrivalCity, int month)
    {
        String sql = "select day(departure_time) day, min(price) min_price, url from flight " +
                "where departure_city = '"+ departureCity +"' and arrival_city = '" + arrivalCity + "' and month(departure_time) = " + month +
                " group by day;";

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql);
        List<DayPredict> dayPredicts = new ArrayList<DayPredict>();

        int lastDay = 0;
        int currentDay = 0;

        for(int i = 0; i < results.size(); i++)
        {
            DayPredict dayPredict = new DayPredict();
            Map<String, Object> map = results.get(i);
            for(String key : map.keySet())
            {
                if(key.equals("day"))
                {
                    currentDay = (Integer) map.get(key);
                    dayPredict.setDay(currentDay);
                }
                else if(key.equals("min_price"))
                {
                    dayPredict.setMin_price((Integer) map.get(key));
                }
                else if(key.equals("url"))
                {
                    dayPredict.setUrl((String) map.get(key));
                }
            }

            for(int j = 0; j < currentDay - lastDay - 1; j++)
            {
                DayPredict dayValueOfZero = new DayPredict();
                dayValueOfZero.setDay(lastDay + j + 1);
                dayValueOfZero.setMin_price(0);
                dayValueOfZero.setUrl("");
                dayPredicts.add(dayValueOfZero);
            }
            dayPredicts.add(dayPredict);
            lastDay = currentDay;
        }

        return dayPredicts;
    }

    @RequestMapping("/monthPredict")
    public String showMonthPredict()
    {
        return "Prediction";
    }

}
