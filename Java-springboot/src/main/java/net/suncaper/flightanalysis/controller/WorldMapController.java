package net.suncaper.flightanalysis.controller;

import net.suncaper.flightanalysis.domain.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class WorldMapController extends HttpServlet {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/searchTicket")
    public String WorldMap(Model model, String departureCity, String departureDate)
    {
        String sql = "select arrival_city, arrival_airport_code, min(price) min_price, url from flight where departure_city = '" + departureCity + "' and departure_time like '" + departureDate +"%' group by arrival_city limit 10;";
        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql);

        List<Ticket> tickets = new ArrayList<Ticket>();

        for(int i = 0; i < results.size(); i++)
        {
            Ticket ticket = new Ticket();
            Map<String, Object> map = results.get(i);
            for(String key : map.keySet())
            {
                if(key.equals("arrival_city"))
                {
                    ticket.setArrival_city((String) map.get(key));
                }
                else if(key.equals("arrival_airport_code"))
                {
                    ticket.setArrival_airport_code((String) map.get(key));
                }
                else if(key.equals("min_price"))
                {
                    ticket.setMin_price((Integer) map.get(key));
                }
                else if(key.equals("url"))
                {
                    ticket.setUrl((String) map.get(key));
                }
            }
            tickets.add(ticket);
        }

        model.addAttribute("ticketInfo", tickets);
/*
        System.out.println(departureCity);
        System.out.println(departureDate);
        System.out.println(results);

        for(int i = 0; i < tickets.size(); i++)
        {
            System.out.println(tickets.get(i).getArrival_city());
            System.out.println(tickets.get(i).getArrival_airport_code());
            System.out.println(tickets.get(i).getMin_price());
            System.out.println(tickets.get(i).getUrl());
        }
*/
        return "WorldMap";
    }

    @RequestMapping("/worldMap")
    public String showWorldMap(Model model)
    {
        return "WorldMap";
    }
}