package net.suncaper.flightanalysis.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import net.suncaper.flightanalysis.domain.DayPredict;
import net.suncaper.flightanalysis.domain.Flight;
import net.suncaper.flightanalysis.domain.MonthPredict;
import org.omg.CORBA.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServlet;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Controller
public class TestController {

    public void makeJSON(JSONObject jsonObject, String departureAirport, String arrivalAirport, String departureDate)
    {
        List<String> departure_airport = new ArrayList<String>();
        departure_airport.add(departureAirport);
        List<String> arrival_airport = new ArrayList<String>();
        arrival_airport.add(arrivalAirport);
        List<String> date = new ArrayList<String>();
        date.add(departureDate);

        jsonObject.put("departure_airport", departure_airport);
        jsonObject.put("arrival_airport", arrival_airport);
        jsonObject.put("date", date);
        jsonObject.put("type", "OneWay");
    }

    public void makeJSONTwoWay(JSONObject jsonObject, String DAirport1, String AAirport1, String DDate1, String DAirport2, String AAirport2, String DDate2)
    {
        List<String> departure_airport = new ArrayList<String>();
        departure_airport.add(DAirport1);
        departure_airport.add(DAirport2);
        List<String> arrival_airport = new ArrayList<String>();
        arrival_airport.add(AAirport1);
        arrival_airport.add(AAirport2);
        List<String> date = new ArrayList<String>();
        date.add(DDate1);
        date.add(DDate2);

        jsonObject.put("departure_airport", departure_airport);
        jsonObject.put("arrival_airport", arrival_airport);
        jsonObject.put("date", date);
        jsonObject.put("type", "TwoWay");
    }

    @RequestMapping("/search")
    public String searchTest(Model model, String departureAirportOne, String arrivalAirportOne, String departureDateOne, String departureAirportTwo, String arrivalAirportTwo, String departureDateTwo) throws IOException {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        headers.setContentType(type);

        JSONObject postJSON = new JSONObject();
        makeJSONTwoWay(postJSON, departureAirportOne, arrivalAirportOne, departureDateOne, departureAirportTwo, arrivalAirportTwo, departureDateTwo);
        System.out.println(postJSON);
        String jsonString = restTemplate.postForObject("http://10.8.0.10:8000/test/", postJSON, String.class);
        System.out.println(jsonString);
/*
        File file = new File("D:\\Software\\IntelliJIDEA\\Project\\flight-analysis\\Java-springboot\\src\\main\\java\\net\\suncaper\\flightanalysis\\controller\\test.txt");
        FileReader fileReader = new FileReader(file);
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bf= new BufferedReader(fileReader);
        String s;
        while((s = bf.readLine())!=null){
            stringBuffer.append(s.trim());
        }
        String jsonString = stringBuffer.toString();
*/
        JSONObject jsonData = JSONObject.parseObject(jsonString);
        JSONArray route_list = jsonData.getJSONArray("route_list");
        System.out.println(route_list);

        List<Flight> flights = new ArrayList<Flight>();
        List<Flight> flightsCat = new ArrayList<Flight>();

        for(int k = 0; k < route_list.size(); k++)
        {
            JSONArray route_list_sub = route_list.getJSONArray(k);
            System.out.println(route_list_sub);
            for(int i = 0; i < route_list_sub.size(); i++)
            {
                JSONObject flightInfo = route_list_sub.getJSONObject(i);
                boolean isCat = flightInfo.getBoolean("auto_cat");
                JSONArray flightsData = flightInfo.getJSONArray("legs");
                for(int j = 0; j < flightsData.size(); j++)
                {
                    JSONObject flightObject = flightsData.getJSONObject(j);
                    Flight flight = JSON.toJavaObject(flightObject, Flight.class);
                    if(isCat)
                    {
                        flightsCat.add(flight);
                    }
                    else
                    {
                        flights.add(flight);
                    }
                }
            }
        }

        model.addAttribute("flights", flights);
        model.addAttribute("flightsCat", flightsCat);

        return "Test";
    }

    @RequestMapping("/test")
    public String showTest()
    {
        return "Test";
    }

}
