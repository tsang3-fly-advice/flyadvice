import requests
from bs4 import BeautifulSoup
import time
from threading import Thread


class Proxy:
    def __init__(self):
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
            'Referer': 'https://www.kuaidaili.com/free/'
        }

    def get_page_html(self, url):
        try:
            result = requests.get(url=url, headers=self.headers)
            result.raise_for_status()
            html = BeautifulSoup(result.text, 'lxml')
            return html
        except:
            print('connection failed！')

    def get_proxy(self, html):
        proxies = []  # 代理池
        trs = html.find('table', class_='table table-bordered table-striped').find('tbody').find_all('tr')
        for tr in trs:
            tcp = list(tr.stripped_strings)[3].lower()
            ip = list(tr.stripped_strings)[0]
            port = list(tr.stripped_strings)[1]
            proxies.append((tcp, ip + ":" + port))
        return proxies

    def test_proxies(self, proxies):
        proxies = proxies
        for proxy in proxies:
            test = Thread(target=self.thread_test_proxy, args=(proxy[0], proxy[1],))
            test.start()

    def thread_test_proxy(self, tcp, address):
        try:
            print('待验证ip：%s' % address)
            result = requests.get('https://www.baidu.com/', headers=self.headers, proxies={tcp: address}, timeout=3)  # timeout 超时时间
            print(result.status_code)
            if result.status_code == 200:
                self.save_proxys((tcp, address))
            else:
                pass
        except Exception as e:
            print(e)

    def save_proxys(self, proxy):
        with open("IP.txt", 'a+') as f:
            print('有效IP：【{}://{}】'.format(proxy[0], proxy[1]))
            f.write('{}://{}'.format(proxy[0], proxy[1]) + '\n')

    def auto_flow(self, base_url, page):
        for i in range(1, page):
            try:
                time.sleep(1)
                url = base_url.format(i)
                proxies = self.get_proxy(self.get_page_html(url))
                self.test_proxies(proxies)
            except Exception as e:
                print('Error type：%s' % e)
                continue


if __name__ == '__main__':
    obj = Proxy()
    obj.auto_flow('https://www.kuaidaili.com/free/inha/{}/', 2)  # url 和爬取分页数
    print('finished')
