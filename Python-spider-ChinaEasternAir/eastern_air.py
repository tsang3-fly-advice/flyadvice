# coding-utf8
import itertools
import multiprocessing
from selenium.webdriver.chrome.options import Options
import proxy_pool
import requests
import pymysql
import pandas as pd
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from lxml import etree
import datetime

connect = pymysql.connect(
    user="root",
    password="",
    port=3306,
    host="localhost",
    db="mysql",
    charset="utf8"
)
conn = connect.cursor()
conn.execute("USE flight_ceair")


class ChinaEasternAir:
    __count = 1
    __driver = None
    __time = None
    __end_time = None
    __code_dict = dict()
    __last_time = None
    __last_dept = None
    __last_arri = None

    def __init__(self):
        # ops = Options()
        # proxy = proxy_pool.getSingleIP()
        # ops.add_argument('--proxy-server=%s' % proxy.get('https')[8:])
        self.__driver = webdriver.Chrome(executable_path='chromedriver.exe')
        self.__driver.implicitly_wait(10)
        self.__driver.maximize_window()
        self.__time = datetime.date.today()
        self.__end_time = self.__time + datetime.timedelta(days=60)
        self.__code_dict = pd.read_csv('国内机场三字码对照表.csv').set_index("机场").to_dict()['三字码']

    def get_msg(self, dept_city, arri_city):
        url = "http://www.ceair.com/"
        self.__driver.get(url)
        input_dept_city = self.__driver.find_element_by_css_selector('#label_ID_0')
        input_dept_city.clear()
        input_dept_city.send_keys(dept_city)
        self.__driver.find_element_by_css_selector(
            'body > div.city-panel > div > div > div > div > div > div.content_body.list2 > ul > li > span.s1').click()
        sleep(1)
        input_arrive_city = self.__driver.find_element_by_css_selector('#label_ID_1')
        input_arrive_city.clear()
        input_arrive_city.send_keys(arri_city)
        self.__driver.find_element_by_css_selector(
            'body > div.city-panel > div > div > div > div > div > div.content_body.list2 > ul > li > span.s1').click()
        sleep(1)
        input_dept_date = self.__driver.find_element_by_css_selector('#depDt')
        self.__driver.execute_script("document.getElementById('depDt').removeAttribute('readonly')")
        sleep(1)
        for i in range(10):
            input_dept_date.send_keys(Keys.BACKSPACE)
        sleep(1)
        input_dept_date.send_keys(self.__time.isoformat())
        # self.__driver.execute_script("document.body.style.zoom='0.5'")
        ActionChains(self.__driver).move_by_offset(10, 10).click().perform()
        sleep(1)
        btn_search = self.__driver.find_element_by_xpath('//*[@id="btn_flight_search"]')
        btn_search.click()
        current_handle = self.__driver.current_window_handle
        all_handles = self.__driver.window_handles
        for handle in all_handles:
            if handle != current_handle:
                self.__driver.switch_to.window(handle)

    def get_content(self):
        sleep(3)
        content = self.__driver.page_source.encode('utf-8')
        response = etree.HTML(content)
        flights = response.xpath("//*[@id='sylvanas_3']//*[@class='flight']")
        print("records: ", len(flights))
        for each in flights:
            flight_name = each.xpath(".//div[1]/span[1]/text()")[0]  # 航空公司
            air_num = each.xpath(".//div[1]/text()")[0][3:]  # 航班号
            approach = each.xpath(".//div[1]/span[2]/text()")[0]  # 飞行方式（直达/转机）
            dept_time_info = each.xpath(".//div[2]/div[1]/time[1]/text()")[0]
            dept_time = dept_time_info[:dept_time_info.find(' ')]  # 出发时间
            dept_info = each.xpath(".//div[2]/div[1]/text()")[0]  # 出发机场、航站楼
            dept_airport = dept_info[:dept_info.find(' ')]  # 出发机场
            dept_terminal = dept_info[dept_info.find(' ') + 1:]  # 出发航站楼
            flight_time = each.xpath(".//dfn/text()")[0]  # 飞行时间
            arrive_time_info = each.xpath(".//div[2]/div[3]/time[1]/text()")[0]
            arrive_time = arrive_time_info[:]  # 到达时间
            arrive_info = each.xpath(".//div[2]/div[@class='airport']/text()")[0]  # 到达机场、航站楼
            arrive_airport = arrive_info[:arrive_info.find(' ')]  # 到达机场
            arrive_terminal = arrive_info[arrive_info.find(' ') + 1:]  # 到达航站楼
            try:
                eco_price = each.xpath(".//section[2]/dl/dd[1]/text()")[0]  # 经济舱价格
            except:
                "no eco class"
                continue
            # preeco_price = each.xpath(".//section[2]/dl/dd[2]/text()")[0]  # 超级经济舱价格
            # luxbus_price = each.xpath(".//section[2]/dl/dd[3]/text()")[0]  # 公务舱/头等舱价格
            dept_city = self.__driver.find_element_by_css_selector("#label_ID_0").get_attribute('value')  # 出发城市
            arrive_city = self.__driver.find_element_by_css_selector("#label_ID_1").get_attribute('value')  # 到达城市
            plane_type = each.xpath(".//*//ul/li[1]/div/div[2]/@acfamily")[0]
            print(">>>>>>>>>>>>>>>>>>", self.__time.isoformat(), ">>>>>>>>>>>>>>>>>")
            print("航空公司：", flight_name)
            print("航班号：", air_num)
            print("飞行方式：", approach)
            print("出发城市：", dept_city)
            print("出发时间：", dept_time)
            print("出发机场：", dept_airport)
            print("出发航站楼：", dept_terminal)
            print("飞行时间：", flight_time)
            print("到达城市：", arrive_city)
            print("到达时间：", arrive_time)
            print("到达机场：", arrive_airport)
            print("到达航站楼：", arrive_terminal)
            print("机型：", plane_type)
            print("经济舱价格：", eco_price)
            # print("超级经济舱价格：", preeco_price)
            # print("公务舱/头等舱价格：", luxbus_price)
            print(">>>>>>>>>>>>>>>>>>", self.__time.isoformat(), ">>>>>>>>>>>>>>>>>")
            purchase_url = self.__driver.current_url
            try:
                dept_airport_code = self.__code_dict[dept_airport]
            except:
                print("dept_airport_code not in list")
                continue

            try:
                arrive_airport_code = self.__code_dict[arrive_airport]
            except:
                print("arrive_airport_code not in list")
                continue

            comp_res = compare_time(self.__time.isoformat() + " " + dept_time + ":00",
                                    self.__time.isoformat() + " " + arrive_time + ":00")
            dept_time = self.__time.isoformat() + " " + dept_time
            if comp_res:
                arrive_time = (self.__time + datetime.timedelta(days=1)).isoformat() + " " + arrive_time
            else:
                arrive_time = self.__time.isoformat() + " " + arrive_time
            record = [air_num, plane_type, flight_name, eco_price, dept_city, dept_time, dept_airport, dept_terminal,
                      dept_airport_code, arrive_city, arrive_time, arrive_airport, arrive_terminal, arrive_airport_code,
                      flight_time, 0 if approach[0] == '直' else 1, 0 if approach[0] == '直' else 1, purchase_url]
            self.save_mysql_msg(record)

    def change_date(self):
        self.__time = self.__time + datetime.timedelta(days=1)

    def validate_date(self):
        if self.__time.__le__(self.__end_time):
            return True
        return False

    def update_msg(self):
        sleep(3)
        input_date = self.__driver.find_element_by_xpath("//*[@id='trip']/dt[1]/section[2]/input")
        for i in range(10):
            input_date.send_keys(Keys.BACKSPACE)
        sleep(0.5)
        input_date.send_keys(self.__time.isoformat())
        ActionChains(self.__driver).move_by_offset(0, 0).click().perform()
        ActionChains(self.__driver).move_by_offset(0, 0).click().perform()
        sleep(1)
        btn_search = self.__driver.find_element_by_id("btn_flight_search")
        btn_search.click()
        sleep(10)  # wait for loading

    def save_mysql_msg(self, record):
        try:
            conn.execute(
                "INSERT INTO flight_info(flight_num, plane_type, airline, price, departure_city, "
                "departure_time, departure_airport, departure_terminal, departure_airport_code,"
                "arrival_city, arrival_time, arrival_airport, arrival_terminal, arrival_airport_code, "
                "flight_time, stop_count, transfer_count, url)VALUES(%s, %s, %s, %s, %s, %s, %s, %s,"
                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", record)
            connect.commit()
            print('upload success: ', self.__count)
            self.__count = self.__count + 1
        except Exception as e:
            print('upload failed ', e)
            pass

    def get_mysql_msg(self):
        try:
            conn.execute(
                "SELECT departure_city, arrival_city, departure_time FROM flight_info ORDER BY id DESC LIMIT 1"
            )
            connect.commit()
            dt = conn.fetchone()
            self.__last_dept = dt[0]
            self.__last_arri = dt[1]
            self.__last_time = datetime.datetime.date(datetime.datetime.strptime(dt[2][0:10], "%Y-%m-%d"))
        except:
            pass
        return self.__last_dept, self.__last_arri, None if self.__last_time is None else self.__last_time.isoformat()

    def get_time(self):
        return self.__time.isoformat(), self.__end_time.isoformat()

    def set_time_to_last(self):
        self.__time = self.__last_time

    def close(self):
        self.__driver.close()


def work_flow(deptCt, arriCt):
    # if deptCt in ['北京', '上海', '广州', '深圳', '成都', '海口', '南京', '重庆', '西安', '长沙']:
    #     return

    c = ChinaEasternAir()
    last_dept, last_arri, last_time = c.get_mysql_msg()
    now_time, end_time = c.get_time()

    if last_arri is None:  # empty
        pass
    # elif deptCt == '杭州' and arriCt == '南宁':
    #     pass
    elif end_time == last_time:  # over
        pass
    elif last_dept != deptCt or last_arri != arriCt:  # not the right city pairs
        print("skip: ", deptCt, arriCt)
        c.close()
        return
    else:  # not over & not empty -> need to continue from latest state
        c.set_time_to_last()

    c.get_msg(dept_city=deptCt, arri_city=arriCt)
    c.get_content()
    c.change_date()
    while c.validate_date():
        c.update_msg()
        c.get_content()
        c.change_date()
    c.close()


def compare_time(start_time, end_time):
    now = datetime.datetime.now()
    d_start = datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
    d_end = datetime.datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
    result = (d_start <= now) and (d_end >= now)
    return result


def cities_load():
    data = pd.read_csv('国内机场三字码对照表.csv')
    search_cities = data['城市'].values.tolist()
    search_cities[1] = '上海'
    del (search_cities[2])
    city_pairs = []
    for i in itertools.permutations(search_cities, 2):
        city_pairs.append(i)
    return city_pairs


if __name__ == '__main__':
    # pool = multiprocessing.Pool(processes=2)
    test_list = cities_load()
    for cities in test_list:
        work_flow(cities[0], cities[1])
    #     pool.apply_async(work_flow, (cities[0], cities[1]))
    # pool.close()
    # pool.join()
    # print("done")
    '''
    每个线程：
    提取地点
    启动线程
    循环日期一年：
        获得信息，存入变量
        保存
    '''
