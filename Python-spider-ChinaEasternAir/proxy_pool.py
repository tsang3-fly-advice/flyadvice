import requests, random, time


def getSingleIP():
    while True:
        time.sleep(random.randint(2, 8))
        try:
            ip = requests.get(url='http://route.xiongmaodaili.com/xiongmao-web/'
                                  'api/glip?secret=14a9240e1a66470864e9114878aa63f8'
                                  '&orderNo=GL20200618011315wTX2auIf&count=1&isTxt=1'
                                  '&proxyType=1',
                              timeout=(3,7)).content.decode(encoding='utf8')
        except requests.exceptions.ConnectTimeout:
            time.sleep(3)
            print("requests.exceptions.ConnectTimeout")
            continue
        except requests.exceptions.ReadTimeout:
            time.sleep(3)
            print("requests.exceptions.ReadTimeout")
            continue
        except requests.exceptions.ConnectionError:
            time.sleep(3)
            print("requests.exceptions.ConnectionError")
            continue
        ip = ip.rstrip('\n')
        ip = ip.rstrip('\r')
        ip = {'https': 'https://' + ip}
        print(ip)
        return ip
