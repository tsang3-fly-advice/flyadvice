import datetime
import pandas as pd
import json

from lxml import etree
from selenium import webdriver

from time import sleep
from transfer import city_code_sw
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def compare_time(start_time, end_time):
	now = datetime.datetime.now()
	d_start = datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
	d_end = datetime.datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
	result = (d_start <= now) and (d_end >= now)
	return result


class Cea_access:
	__driver = None
	__time = None
	# __code_dict = dict()
	__routelist = []

	def __init__(self):
		self.__driver = webdriver.Chrome(executable_path='chromedriver.exe')
		self.__driver.implicitly_wait(10)
		self.__driver.maximize_window()
		self.__time = datetime.date.today()
		# self.__code_dict = pd.read_csv('国内机场三字码对照表.csv').set_index('机场').to_dict()['三字码']
		# foreign_code = pd.read_csv('国外及港澳台机场三字码对照表.csv').set_index('机场').to_dict()['三字码']
		# self.__code_dict.update(foreign_code)

	def get_msg(self, dept_city, arri_city, dept_time):
		self.__time = datetime.date(int(dept_time.split('-')[0]), int(dept_time.split('-')[1]), int(dept_time.split('-')[2]))
		url = "http://www.ceair.com/"
		self.__driver.get(url)
		input_dept_city = self.__driver.find_element_by_css_selector('#label_ID_0')
		input_dept_city.clear()
		input_dept_city.send_keys(dept_city)
		self.__driver.find_element_by_css_selector(
			'body > div.city-panel > div > div > div > div > div > div.content_body.list2 > ul > li > span.s1').click()
		sleep(1)
		input_arrive_city = self.__driver.find_element_by_css_selector('#label_ID_1')
		input_arrive_city.clear()
		input_arrive_city.send_keys(arri_city)
		self.__driver.find_element_by_css_selector(
			'body > div.city-panel > div > div > div > div > div > div.content_body.list2 > ul > li > span.s1').click()
		sleep(1)
		input_dept_date = self.__driver.find_element_by_css_selector('#depDt')
		self.__driver.execute_script("document.getElementById('depDt').removeAttribute('readonly')")
		sleep(1)
		for i in range(10):
			input_dept_date.send_keys(Keys.BACKSPACE)
		sleep(1)
		input_dept_date.send_keys(self.__time.isoformat())
		ActionChains(self.__driver).move_by_offset(10, 10).click().perform()
		sleep(1)
		btn_search = self.__driver.find_element_by_xpath('//*[@id="btn_flight_search"]')
		btn_search.click()
		current_handle = self.__driver.current_window_handle
		all_handles = self.__driver.window_handles
		for handle in all_handles:
			if handle != current_handle:
				self.__driver.switch_to.window(handle)

	def get_content(self):
		sleep(3)
		content = self.__driver.page_source.encode('utf-8')
		response = etree.HTML(content)
		flights = response.xpath("//*[@id='sylvanas_3']//*[@class='flight']")
		print("records: ", len(flights))
		for each in flights:
			flight_name = each.xpath(".//div[1]/span[1]/text()")[0]  # 航空公司
			air_num = each.xpath(".//div[1]/text()")[0][3:]  # 航班号
			approach = each.xpath(".//div[1]/span[2]/text()")[0]  # 飞行方式（直达/转机）
			dept_time_info = each.xpath(".//div[2]/div[1]/time[1]/text()")[0]
			dept_time = dept_time_info[:dept_time_info.find(' ')]  # 出发时间
			dept_info = each.xpath(".//div[2]/div[1]/text()")[0]  # 出发机场、航站楼
			dept_airport = dept_info[:dept_info.find(' ')]  # 出发机场
			dept_terminal = dept_info[dept_info.find(' ') + 1:]  # 出发航站楼
			flight_time = each.xpath(".//dfn/text()")[0]  # 飞行时间
			arrive_time_info = each.xpath(".//div[2]/div[3]/time[1]/text()")[0]
			arrive_time = arrive_time_info[:]  # 到达时间
			arrive_info = each.xpath(".//div[2]/div[@class='airport']/text()")[0]  # 到达机场、航站楼
			arrive_airport = arrive_info[:arrive_info.find(' ')]  # 到达机场
			arrive_terminal = arrive_info[arrive_info.find(' ') + 1:]  # 到达航站楼
			try:
				eco_price = each.xpath(".//section[2]/dl/dd[1]/text()")[0]  # 经济舱价格
			except:
				"no eco class"
				eco_price = "--"
			# preeco_price = each.xpath(".//section[2]/dl/dd[2]/text()")[0]  # 超级经济舱价格
			# luxbus_price = each.xpath(".//section[2]/dl/dd[3]/text()")[0]  # 公务舱/头等舱价格
			dept_city = self.__driver.find_element_by_css_selector("#label_ID_0").get_attribute('value')  # 出发城市
			arrive_city = self.__driver.find_element_by_css_selector("#label_ID_1").get_attribute('value')  # 到达城市
			plane_type = each.xpath(".//*//ul/li[1]/div/div[2]/@acfamily")[0]
			purchase_url = self.__driver.current_url

			try:
				dept_airport_code = city_code_sw[dept_airport]
			except:
				print("dept_airport_code not in list")
				dept_airport_code = ""

			try:
				arrive_airport_code = city_code_sw[arrive_airport]
			except:
				print("arrive_airport_code not in list")
				arrive_airport_code = ""
			# print(">>>>>>>>>>>>>>>>>>", self.__time.isoformat(), ">>>>>>>>>>>>>>>>>")
			# print("航空公司：", flight_name)
			# print("航班号：", air_num)
			# print("飞行方式：", approach)
			# print("出发城市：", dept_city)
			# print("出发时间：", dept_time)
			# print("出发机场：", dept_airport)
			# print("出发航站楼：", dept_terminal)
			# print("飞行时间：", flight_time)
			# print("到达城市：", arrive_city)
			# print("到达时间：", arrive_time)
			# print("到达机场：", arrive_airport)
			# print("到达航站楼：", arrive_terminal)
			# print("机型：", plane_type)
			# print("经济舱价格：", eco_price)
			# print("超级经济舱价格：", preeco_price)
			# print("公务舱/头等舱价格：", luxbus_price)
			# print(">>>>>>>>>>>>>>>>>>", self.__time.isoformat(), ">>>>>>>>>>>>>>>>>")

			comp_res = compare_time(self.__time.isoformat() + " " + dept_time + ":00",
			                        self.__time.isoformat() + " " + arrive_time + ":00")
			dept_time = self.__time.isoformat() + " " + dept_time
			if comp_res:
				arrive_time = (self.__time + datetime.timedelta(days=1)).isoformat() + " " + arrive_time
			else:
				arrive_time = self.__time.isoformat() + " " + arrive_time
			legs = [{
				"flight_num": air_num[0:5],
				"plane_type": plane_type,
				"airline": flight_name,
				"price": eco_price,
				"departure_city": dept_city,
				"departure_time": dept_time,
				"departure_airport": dept_airport,
				"departure_terminal": dept_terminal,
				"departure_airport_code": dept_airport_code,
				"arrival_city": arrive_city,
				"arrival_time": arrive_time,
				"arrival_airport": arrive_airport,
				"arrival_terminal": arrive_terminal,
				"arrival_airport_code": arrive_airport_code,
				"flight_time": flight_time,
				"stop_count": 0 if approach[0] == '直' else 1,
				"transfer_count": 0 if approach[0] == '直' else 1,
				"url": purchase_url
			}]
			single_route = {
				"legs": legs,
				"auto_cat": False
			}
			self.__routelist.append(single_route)

	def get_json_msg(self, deptCt, arriCt, deptTm):
		self.get_msg(deptCt, arriCt, deptTm)
		self.get_content()
		routes = {
			"route_list": self.__routelist
		}
		return json.dumps(routes, ensure_ascii=False)


c = Cea_access()
print(c.get_json_msg("PVG", "PEK", "2020-07-07"))
