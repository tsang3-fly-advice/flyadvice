import requests, json,time,datetime
import hashlib
import re
from RealtimeSearch import proxy_pool
import  random
from fake_useragent import UserAgent
import csv
# 此处的参数是json   出发三字码，达到三字码，出发时间，成人数，儿童数，婴儿数,三字码请传小写的

flight_tickets = []

cisborder_airport = []
abroad_airport = []

ip_proxy = proxy_pool.getSingleIP()

def read_csv():
    csv_file = csv.reader(open('国内机场三字码对照表.csv', 'r',encoding='utf-8'))
    for line in csv_file:
        cisborder_airport.append(line[1])
    csv_file = csv.reader(open('国外及港澳台机场三字码对照表.csv', 'r', encoding='utf-8'))
    for line in csv_file:
        abroad_airport.append(line[1])

    print(cisborder_airport)
    print(abroad_airport)


def get_index(paramter):
    while True:
        # 如果只执行这个脚本，需要将下边这行注释掉，如果放到服务，就打开
        # paramter = json.loads(paramter)
        global ip_proxy
        dep_code = paramter["dep_code"]
        arr_code = paramter["arr_code"]
        date = paramter["date"]
        adult = paramter["adult"]
        child = paramter["child"]
        infant = paramter["infant"]
        urls = 'https://flights.ctrip.com/international/search/oneway-{}-{}?depdate={}&cabin=y_s&adult={}&child={}&infant={}'.format(
            dep_code, arr_code, date, adult, child, infant)

        # 这个url返回所需要的参数
        # urls = 'https://flights.ctrip.com/international/search/oneway-bjs-sel?depdate=2019-05-21&cabin=y_s&adult=1&child=0&infant=0'
        headers = {
           'user-agent': str(UserAgent().random)
        }
        try:
            response = requests.get(urls, proxies=ip_proxy, headers=headers,timeout=(3,7))
        except :
            ip_proxy = proxy_pool.getSingleIP()
            continue
        try:
            data = re.findall(r'GlobalSearchCriteria =(.+);', response.text)[0]
            post_dict = json.loads(data)
        except:
            continue
        return post_dict, date, urls


def get_data(post_dicts):
    global ip_proxy
    count_error = 0
    while True:
        post_dict = post_dicts[0]
        date = post_dicts[1]
        # ABTString = re.findall('id="ab_testing_tracker" value=(.+)/>', response.text)[0]
        trans_id = post_dict['transactionID']
        url = "https://flights.ctrip.com/international/search/api/search/batchSearch"

        # 拼接加密参数 trans_id +出发地到达地+时间
        sign_value = trans_id + post_dict['flightSegments'][0]['departureCityCode'] + post_dict['flightSegments'][0][
            'arrivalCityCode'] + date
        # 进行md5加密
        md5 = hashlib.md5()
        md5.update(sign_value.encode('utf-8'))
        sign = md5.hexdigest()

        flightWayEnum = "OW"
        arrivalProvinceId = post_dict["flightSegments"][0]["arrivalProvinceId"]
        arrivalCountryName = post_dict["flightSegments"][0]["arrivalCountryName"]
        cabinEnum = post_dict["cabin"]
        departCountryName = post_dict["flightSegments"][0]["departureCountryName"]
        segmentNo = len(post_dict["flightSegments"])
        departureCityId = post_dict["flightSegments"][0]["departureCityId"]
        isMultiplePassengerType = 0

        post_dict["flightWayEnum"] = flightWayEnum
        post_dict["arrivalProvinceId"] = arrivalProvinceId
        post_dict["arrivalCountryName"] = arrivalCountryName
        post_dict["cabinEnum"] = cabinEnum
        post_dict["departCountryName"] = departCountryName
        post_dict["segmentNo"] = segmentNo
        post_dict["departureCityId"] = departureCityId
        post_dict["isMultiplePassengerType"] = isMultiplePassengerType

        # payload = '{"flightWayEnum":"OW","arrivalProvinceId":0,"arrivalCountryName":"韩国","infantCount":0,"cabin":"Y_S","cabinEnum":"Y_S","departCountryName":"中国","flightSegments":[{"departureDate":"2019-05-21","arrivalProvinceId":0,"arrivalCountryName":"韩国","departureCityName":"北京","departureCityCode":"BJS","departureCountryName":"中国","arrivalCityName":"首尔","arrivalCityCode":"SEL","departureCityTimeZone":480,"arrivalCountryId":42,"timeZone":480,"departureCityId":1,"departureCountryId":1,"arrivalCityTimeZone":540,"departureProvinceId":1,"arrivalCityId":274}],"childCount":0,"segmentNo":1,"adultCount":1,"extensionAttributes":{},"transactionID":"c9ab78578e8342e8ba1101e5104fc5bd","directFlight":false,"departureCityId":1,"isMultiplePassengerType":0,"flightWay":"S","arrivalCityId":274,"departProvinceId":1}'
        # payload = payload.encode('UTF-8')
        headers = {
            'origin': "https://flights.ctrip.com",
            'sign': sign,
            'user-agent': str(UserAgent().random),
            'content-type': "application/json;charset=UTF-8",
            'accept': "application/json",
            'transactionid': trans_id,
            'Host': "flights.ctrip.com",
            'content-length': "815",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        try:
            response = requests.request("POST", url, data=json.dumps(post_dict), proxies=ip_proxy, headers=headers,timeout=(3,7))
        except :
            ip_proxy = proxy_pool.getSingleIP()
            continue
        response_json = json.loads(response.text)
        if 'data' in response_json:
            search_id = response_json['data']['context']['searchId']
            finished = response_json['data']['context']['finished']
        else:
            continue

        if 'searchCriteriaToken' in response_json['data']['context'] \
                and response_json['data']['context']['searchCriteriaToken'] != '':
            print('batch:',end='')
            return response_json, post_dicts[2]

        if search_id != "":
            url = 'https://flights.ctrip.com/international/search/api/search/pull/' + search_id
            try:
                response = requests.request("POST", url, data=json.dumps(post_dict), proxies=ip_proxy, headers=headers,timeout=(3,7))
            except:
                ip_proxy = proxy_pool.getSingleIP()
                continue
            response_json = json.loads(response.text)

            if 'searchCriteriaToken' in response_json['data']['context'] and \
                    (json.loads(response.text))['data']['context']['finished'] and \
                    'flightItineraryList' in response_json['data']:
                print('more:',end='')
                return response_json, post_dicts[2]

        count_error += 1
        if count_error > 3:
            print(post_dicts)
            return None


def parse_json(response_json,url,call_type=0):
    print(response_json)
    result_list = []
    if 'flightItineraryList' in response_json['data']:
        for flight in response_json['data']['flightItineraryList']:
            try:
                data = []
                flight_num = (flight['itineraryId']).replace(',',' ')
                plane_type = ''
                for each_type in flight['flightSegments'][0]['flightList']:
                    plane_type += each_type['aircraftName']+' '
                plane_type = plane_type.rstrip()
                airline = flight['flightSegments'][0]['airlineName']
                price = flight['priceList'][0]['adultPrice']
                departure_city = flight['flightSegments'][0]['flightList'][0]['departureCityName']
                departure_time = flight['flightSegments'][0]['flightList'][0]['departureDateTime']
                departure_airport = flight['flightSegments'][0]['flightList'][0]['departureAirportName']
                departure_terminal = flight['flightSegments'][0]['flightList'][0]['arrivalTerminal']
                departure_airport_code = flight['flightSegments'][0]['flightList'][0]['departureAirportCode']
                flight_list_len = len(flight['flightSegments'][0]['flightList']) - 1
                arrival_city = flight['flightSegments'][0]['flightList'][flight_list_len]['arrivalCityName']
                arrival_time = flight['flightSegments'][0]['flightList'][flight_list_len]['arrivalDateTime']
                arrival_airport = flight['flightSegments'][0]['flightList'][flight_list_len]['arrivalAirportName']
                arrival_terminal = flight['flightSegments'][0]['flightList'][flight_list_len]['arrivalTerminal']
                arrival_airport_code = flight['flightSegments'][0]['flightList'][flight_list_len]['arrivalAirportCode']
                flight_time = datetime.datetime.strptime(arrival_time, '%Y-%m-%d %H:%M:%S') - datetime.datetime.strptime(departure_time, '%Y-%m-%d %H:%M:%S')
                stop_count = flight['flightSegments'][0]['stopCount']
                transfer_count = flight['flightSegments'][0]['transferCount']
            except KeyError:
                continue
            data.append(flight_num)
            data.append(plane_type)
            data.append(airline)
            data.append(price)
            data.append(departure_city)
            data.append(departure_time)
            data.append(departure_airport)
            data.append(departure_terminal)
            data.append(departure_airport_code)
            data.append(arrival_city)
            data.append(arrival_time)
            data.append(arrival_airport)
            data.append(arrival_terminal)
            data.append(arrival_airport_code)
            data.append(flight_time)
            data.append(stop_count)
            data.append(transfer_count)
            data.append(url)
            print("save:", end='')
            if call_type == 0:
                save_data(data)
            result_list.append(data)

    return result_list


def save_data(data):
    csv_file = csv.writer(open('international_result.csv', 'a+', encoding='utf-8', newline=''))
    csv_file.writerow(data)
    print(data)

'''
if __name__ == '__main__':
    read_csv()
    for ca in range(0,len(cisborder_airport)):
        for aa in range(0,len(abroad_airport)):
            for i in range(0,360):

                if (ca < 4) or (ca == 4 and aa <= 17) or (ca == 4 and aa == 18 and i < 95):
                    continue

                para = {"dep_code": cisborder_airport[ca], "arr_code": abroad_airport[aa],
                        "date": (datetime.datetime.now()+datetime.timedelta(days=i)).strftime("%Y-%m-%d"),
                        "adult": 1, "child": 0, "infant": 0}
                post_dicts = get_index(para)
                result_json = get_data(post_dicts)
                if result_json is not None:
                    parse_json(result_json[0], result_json[1])
'''
