from django.shortcuts import render
from django.http import HttpResponse,Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.db.models import Q,Count
import urllib
import json,datetime
import time
import re, operator
import fake_useragent
from RealtimeSearch import xiecheng_international, xiecheng_inland, models

arg_list = ['flight_num', 'plane_type', 'airline', 'price', 'departure_city', 'departure_time', 'departure_airport',
            'departure_terminal', 'departure_airport_code', 'arrival_city', 'arrival_time', 'arrival_airport',
            'arrival_terminal', 'arrival_airport_code', 'flight_time', 'stop_count', 'transfer_count', 'url']


def run_inland_crawl(departure_airport_code, arrival_airport_code, date):
    ticket_list = xiecheng_inland.parse_info(departure_airport_code, arrival_airport_code, date,
                                             fake_useragent.UserAgent().random, call_type=1)
    ticket_dict_list = []
    mean_price = 0
    valid_ticket = 0
    for ticket in ticket_list:
        ticket[14] = str(ticket[14])
        if ticket[16] == 0:
            ticket[0] = ticket[0].rstrip(';')
            ticket_dict = {'legs': [], 'auto_cat': False}
            ticket_dict['legs'].append(dict(zip(arg_list, ticket)))
            ticket_dict_list.append(ticket_dict)
            mean_price += ticket[3]
            valid_ticket += 1

    return ticket_dict_list, mean_price/valid_ticket if valid_ticket != 0 else 0xffffffff


def run_international_crawl(departure_airport_code, arrival_airport_code, date):
    para = {"dep_code": departure_airport_code, "arr_code": arrival_airport_code,
            "date": date,"adult": 1, "child": 0, "infant": 0}
    post_dicts = xiecheng_international.get_index(para)
    result_json = xiecheng_international.get_data(post_dicts)
    ticket_dict_list = []
    mean_price = 0
    valid_ticket = 0
    if result_json is not None:
        ticket_list = xiecheng_international.parse_json(result_json[0], result_json[1])
        for ticket in ticket_list:
            ticket[14] = str(ticket[14])
            if ticket[16] == 0:
                ticket_dict = {'legs': [], 'auto_cat': False}
                ticket_dict['legs'].append(dict(zip(arg_list, ticket)))
                ticket_dict_list.append(ticket_dict)
                mean_price += ticket[3]
                valid_ticket += 1

    return ticket_dict_list, mean_price/valid_ticket if valid_ticket != 0 else 0xffffffff


def auto_cat_airline(departure_airport_code, arrival_airport_code, date, mean_price):
    first_ticket_filter = models.Flight.objects.filter(departure_airport_code__iexact=departure_airport_code,
                                                       departure_time__contains=date, price__lte=mean_price).values()
    ticket_dict_list = []
    for first_ticket in first_ticket_filter:

        second_departure_time_min = first_ticket['arrival_time']
        second_departure_time_max = (datetime.datetime.strptime(second_departure_time_min, '%Y-%m-%d %H:%M:%S') +
                                     datetime.timedelta(hours=6)).strftime("%Y-%m-%d")
        second_ticket_filter = models.Flight.objects.filter(departure_airport_code__iexact=
                                                            first_ticket['arrival_airport_code'],
                                                            arrival_airport_code__iexact=arrival_airport_code,
                                                            departure_time__gte=second_departure_time_min,
                                                            departure_time__lte=second_departure_time_max,
                                                            price__lte=mean_price-first_ticket['price']).values()
        for second_ticket in second_ticket_filter:
            ticket_dict = {'legs': [], 'auto_cat': True}
            ticket_dict['legs'].append(first_ticket)
            ticket_dict['legs'].append(second_ticket)
            ticket_dict_list.append(ticket_dict)

    ticket_dict_list = sorted(ticket_dict_list, key=lambda keys: (keys['legs'][0]['price'] + keys['legs'][1]['price']))

    return ticket_dict_list[0:5]


@csrf_exempt
def get_realtime_search(request):
    if request.method == 'POST':
        try:
            result = {'route_list': []}
            search_json = json.loads(request.body)
            print(search_json)
            first_departure = models.Airport.objects.get(airport_name=search_json['departure_airport'][0])
            first_arrival = models.Airport.objects.get(airport_name=search_json['arrival_airport'][0])
            first_date = search_json['date'][0]

            if first_departure.is_inland and first_arrival.is_inland:
                first_crawls_result, mean_price = run_inland_crawl(first_departure.airport_code, first_arrival.airport_code,
                                                       first_date)

            else:
                first_crawls_result, mean_price = run_international_crawl(first_departure.airport_code, first_arrival.airport_code,
                                                              first_date)

            first_auto_cat_result = auto_cat_airline(first_departure.airport_code, first_arrival.airport_code,
                                                     first_date, mean_price)

            first_crawls_result = sorted(first_crawls_result, key=lambda keys: keys['legs'][0]['price'])

            first_result = first_auto_cat_result + first_crawls_result
            result['route_list'].append(first_result)

            if search_json['type'] != 'OneWay':
                second_departure = models.Airport.objects.get(airport_name=search_json['departure_airport'][1])
                second_arrival = models.Airport.objects.get(airport_name=search_json['arrival_airport'][1])
                second_date = search_json['date'][1]

                if second_departure.is_inland and second_arrival.is_inland:
                    second_crawls_result, mean_price = run_inland_crawl(second_departure.airport_code, second_arrival.airport_code,
                                                            second_date)
                else:
                    second_crawls_result, mean_price = run_international_crawl(second_departure.airport_code,
                                                                   second_arrival.airport_code, second_date)

                second_auto_cat_result = auto_cat_airline(second_departure.airport_code, second_arrival.airport_code,
                                                          second_date, mean_price)

                second_crawls_result = sorted(second_crawls_result, key=lambda keys: keys['legs'][0]['price'])

                second_result = second_auto_cat_result + second_crawls_result
                result['route_list'].append(second_result)

            return HttpResponse(json.dumps(result, ensure_ascii=False),
                                content_type="application/json;charset=utf-8")

        except json.JSONDecodeError:
            error = {"valid": 0, "error": "json 格式有误"}
            print(error)
            return HttpResponse(json.dumps(error, ensure_ascii=False),
                                content_type="application/json;charset=utf-8")
        except KeyError:
            error = {"valid": 0, "error": "json 入参有误"}
            print(error)
            return HttpResponse(json.dumps(error, ensure_ascii=False),
                                content_type="application/json;charset=utf-8")
        except IndexError:
            error = {"valid": 0, "error": "出行方式与入参不匹配（单程、往返、多程）"}
            print(error)
            return HttpResponse(json.dumps(error, ensure_ascii=False),
                                content_type="application/json;charset=utf-8")
        except models.Airport.DoesNotExist:
            error = {"valid": 0, "error": "不存在目标机场"}
            print(error)
            return HttpResponse(json.dumps(error, ensure_ascii=False),
                                content_type="application/json;charset=utf-8")

    if request.method == 'GET':
        error = {"valid": 0, "error": "只允许POST请求"}
        return HttpResponse(json.dumps(error, ensure_ascii=False),
                            content_type="application/json.charset=utf-8")




