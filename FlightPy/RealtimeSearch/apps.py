from django.apps import AppConfig


class RealtimesearchConfig(AppConfig):
    name = 'RealtimeSearch'
