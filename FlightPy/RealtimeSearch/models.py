# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Airport(models.Model):
    airport_name = models.CharField(max_length=255, blank=True, null=True)
    airport_code = models.CharField(primary_key=True, max_length=255)
    city_name = models.CharField(max_length=255, blank=True, null=True)
    is_inland = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'airport'


class Flight(models.Model):
    flight_num = models.CharField(primary_key=True, max_length=64)
    plane_type = models.CharField(max_length=64, blank=True, null=True)
    airline = models.CharField(max_length=64, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    departure_city = models.CharField(max_length=64, blank=True, null=True)
    departure_time = models.CharField(max_length=64)
    departure_airport = models.CharField(max_length=64, blank=True, null=True)
    departure_terminal = models.CharField(max_length=64, blank=True, null=True)
    departure_airport_code = models.CharField(max_length=64, blank=True, null=True)
    arrival_city = models.CharField(max_length=64, blank=True, null=True)
    arrival_time = models.CharField(max_length=64, blank=True, null=True)
    arrival_airport = models.CharField(max_length=64, blank=True, null=True)
    arrival_terminal = models.CharField(max_length=64, blank=True, null=True)
    arrival_airport_code = models.CharField(max_length=64, blank=True, null=True)
    flight_time = models.CharField(max_length=64, blank=True, null=True)
    stop_count = models.IntegerField(blank=True, null=True)
    transfer_count = models.IntegerField(blank=True, null=True)
    url = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'flight'
        unique_together = (('flight_num', 'departure_time'),)


