import requests, json,time,datetime
from RealtimeSearch import proxy_pool
from fake_useragent import UserAgent
import csv

cisborder_airport = []

ip_proxy = proxy_pool.getSingleIP()

count_null = 0


def read_csv():
    csv_file = csv.reader(open('国内机场三字码对照表.csv', 'r',encoding='utf-8'))
    for line in csv_file:
        cisborder_airport.append(line[1])
    print(cisborder_airport)


def save_data(data):
    csv_file = csv.writer(open('inland_result.csv', 'a+', encoding='utf-8', newline=''))
    csv_file.writerows(data)
    print(data)


def get_token_response(from_city,to_city,date,agent):
    global ip_proxy
    dcity = from_city
    acity = to_city
    referer = "https://flights.ctrip.com/itinerary/oneway/" + dcity + "-" + acity + "?date=" + date
    url = "https://flights.ctrip.com/itinerary/api/12808/records"
    error_count = 0
    while True:
        if error_count > 3:
            error_count = 0
            ip_proxy = proxy_pool.getSingleIP()
        headers = {
            "User-Agent": agent,
            "Referer": referer,
            "Content-Type": "application/json"}
        request_payload = {
            "vid": "1586849490183.3km93i"
        }
        # post请求
        try:
            response = requests.post(url, proxies=ip_proxy,data=json.dumps(request_payload), headers=headers,timeout=(3,7)).text
            if response is None:
                error_count += 1
                continue
        except:
            ip_proxy = proxy_pool.getSingleIP()
            continue
        return response, referer


#获取token
def get_token(from_city,to_city,date,agent):
    token_response,url = get_token_response(from_city,to_city,date,agent)
    try:
        data_info_str = json.loads(token_response).get('data')[0].get('data')
        token = data_info_str.replace("\\","").split("\"token\":")[1].split("\"")[1]
        return token, url
    except json.decoder.JSONDecodeError:
        return None, url


# 获取返回结果
def get_response(from_city,to_city,date,agent):
    global ip_proxy
    dcity = from_city
    acity = to_city
    error_count = 0
    while True:
        token, token_url = get_token(from_city, to_city, date,agent)
        url = "https://flights.ctrip.com/itinerary/api/12808/products"
        headers = {
            "User-Agent": agent,
            "Referer": "https://flights.ctrip.com/itinerary/oneway/" + dcity + "-" + acity + "?date=" + date,
            "Content-Type": "application/json"}
        request_payload = {
            "flightWay": "Oneway",
            "classType": "ALL",
            "hasChild": False,
            "hasBaby": False,
            "searchIndex": 1,
            "airportParams": [
                {"dcity": dcity, "acity": acity, "date": date}],
            "token": token
        }
        try:
            response = requests.post(url, proxies=ip_proxy,data=json.dumps(request_payload), headers=headers,timeout=(3,7)).text
            route_list = json.loads(response).get('data').get('routeList')
            error = json.loads(response).get('data').get('error')
            if error is not None:
                ip_proxy = proxy_pool.getSingleIP()
                print('查询异常，切换IP '+dcity+' '+acity+' '+date)
                continue
            if route_list is None:
                global count_null
                count_null += 1
                print('None route list ' + dcity + ' ' + acity + ' ' + date)
                return None
        except:
            ip_proxy = proxy_pool.getSingleIP()
            continue

        return route_list,token_url


# 解析返回结果
def parse_info(from_city,to_city,date,agent,call_type=0):
    update_ip()
    result = get_response(from_city,to_city,date,agent)
    flight_tickets = []
    if result is not None:
        route_list, token_url = result[0], result[1]
        for route in route_list:
            try:
                list_len = len(route.get('legs'))-1
                if list_len >= 0 and (route['routeType'] == 'Transit' or route['routeType'] == 'Flight'):
                    ticket = []
                    legs = route.get('legs')
                    flight = legs[0].get('flight')
                    # 提取想要的信息
                    airlineName = flight.get('airlineName')  # 航空公司

                    flightNumber = ''
                    craftTypeName = ''
                    stopCount = 0
                    for leg in legs:
                        flightNumber += leg.get('flight').get('flightNumber')+';'  # 航班编号
                        craftTypeKindDisplayName = leg.get('flight').get('craftTypeKindDisplayName')  # 飞机型号：大型；中型，小型
                        craftTypeName += leg.get('flight').get('craftTypeName') + '(' + craftTypeKindDisplayName + ')'+';'  # 飞机类型
                        stopCount += leg.get('flight').get('stopTimes')

                    flightNumber = flightNumber.rstrip()
                    craftTypeName = craftTypeName.rstrip()

                    departureDate = flight.get('departureDate')  # 出发时间
                    departureCityName = flight.get('departureAirportInfo').get('cityName')  # 出发城市
                    departureAirportName = flight.get('departureAirportInfo').get('airportName')  # 出发机场名称
                    departureTerminalName = flight.get('departureAirportInfo').get('terminal').get('name')  # 出发机场航站楼

                    flight = legs[list_len].get('flight')

                    arrivalCityName = flight.get('arrivalAirportInfo').get('cityName')  # 到达城市
                    arrivalAirportName = flight.get('arrivalAirportInfo').get('airportName')  # 到达机场名称
                    arrivalTerminalName = flight.get('arrivalAirportInfo').get('terminal').get('name')  # 到达机场航站楼
                    arrivalDate = flight.get('arrivalDate')  # 到达时间
                    characteristic = legs[0].get('characteristic')

                    lowestPrice = characteristic.get('lowestPrice')  # 成人经济舱最低价

                    if lowestPrice is None and route['routeType'] == 'Transit':
                        lowestPrice = route['transitPrice']

                    FlightTime = datetime.datetime.strptime(arrivalDate, '%Y-%m-%d %H:%M:%S') - datetime.datetime.strptime(
                        departureDate, '%Y-%m-%d %H:%M:%S')

                    transferCount = list_len

                    ticket.append(flightNumber)
                    ticket.append(craftTypeName)
                    ticket.append(airlineName)
                    ticket.append(lowestPrice)

                    ticket.append(departureCityName)
                    ticket.append(departureDate)
                    ticket.append(departureAirportName)
                    ticket.append(departureTerminalName)
                    ticket.append(from_city)

                    ticket.append(arrivalCityName)
                    ticket.append(arrivalDate)
                    ticket.append(arrivalAirportName)
                    ticket.append(arrivalTerminalName)
                    ticket.append(to_city)

                    ticket.append(FlightTime)
                    ticket.append(stopCount)
                    ticket.append(transferCount)
                    ticket.append(token_url)

                    flight_tickets.append(ticket)
            except:
                continue

    if call_type == 0 and len(flight_tickets) != 0:
        save_data(flight_tickets)

    return flight_tickets


def update_ip():
    global ip_proxy
    ip_proxy = proxy_pool.getSingleIP()


'''
if __name__ == "__main__":
    read_csv()
    for dc in range(0, len(cisborder_airport)):
        for ac in range(0, len(cisborder_airport)):
            if dc != ac:
                for i in range(0, 360):
                    if (dc < 0) or (dc == 0 and ac < 0) or (dc == 0 and ac == 0 and i < 0):  # 断点续传
                        continue
                    if (dc == 1 and ac == 2) or (ac == 1 and dc == 2) or dc == 2:
                        continue

                    agent = str(UserAgent().random)

                    if count_null >= 30:
                        count_null = 0
                        ip_proxy = proxy_pool.getSingleIP()

                    from_city = cisborder_airport[dc].lower()
                    to_city = cisborder_airport[ac].lower()
                    date = (datetime.datetime.now()+datetime.timedelta(days=i)).strftime("%Y-%m-%d")
                    parse_info(from_city,to_city,date,agent)
'''


