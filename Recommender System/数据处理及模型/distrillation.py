from dataloader import AbstactReasoning
from torch.utils.data import DataLoader
import model
from model import *
import torch.optim as optim
import torch
from tqdm import tqdm
import torch.nn.functional as F
import os
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable

#teacher为前两行的

class teacher_model(nn.Module):
    def __init__(self):
        super(teacher_model, self).__init__()
        self.resblock = ResBlock(64, 64, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(64)))
        self.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn64 = nn.BatchNorm2d(64)
        self.bn128 = nn.BatchNorm2d(128)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.relu = nn.ReLU(inplace=True)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.summary = nn.Sequential(nn.Linear(64, 64), nn.ReLU(), nn.Linear(64, 8))

    def forward(self, x, target, meta_target):
        x = x.float()
        meta_target = meta_target.float()

        N, _, H, W = x.shape

        candidate = x[:, :8, :, :]
        features = candidate.contiguous().view(-1, 160, 160).unsqueeze(1)  # (N, 8, 160, 160) -> (8N, 1, 160, 160)

        features = self.maxpool(self.relu(self.bn64(self.conv1(features))))  # -> (8N, 64, 40, 40)
        features = features.view(-1, 8, 64, 40, 40)  # -> (N, 8, 64, 40, 40)

        row1 = torch.sum(features[:, :3, :, :, :], dim=1)  # (N, 64, 40, 40)
        row2 = torch.sum(features[:, 3:6, :, :, :], dim=1)
        row3 = features[:, 6:8, :, :, :]

        row1 = self.resblock(row1)  # -> (N, 64, 20, 20)
        row2 = self.resblock(row2)  # -> (N, 64, 20, 20)

        row_feature = torch.cat((row1, row2), dim = 0) # -> (2N, 64, 20, 20)

        final_row = row_feature[:N, :, :, :] + row_feature[N:, :, :, :] # -> (N, 64, 20, 20)

        final_row = self.avgpool(final_row).view(N, 64) # -> (N, 64, 1, 1) -> (N, 64)

        final_row = self.summary(final_row) # -> (N, 8)

        target = torch.zeros(N, 8).cuda().scatter_(1, target.view(-1, 1), 1)
        teacher_loss = F.binary_cross_entropy_with_logits(final_row, target)

        return final_row, teacher_loss

# student为第三行的

class student_model(nn.Module):
    def __init__(self):
        super(student_model, self).__init__()
        self.resblock = ResBlock(64, 64, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(64)))
        self.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn64 = nn.BatchNorm2d(64)
        self.bn128 = nn.BatchNorm2d(128)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.relu = nn.ReLU(inplace=True)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.summary = nn.Sequential(nn.Linear(64, 64), nn.ReLU(), nn.Linear(64, 8))
        self.mlp_meta = nn.Sequential(nn.Linear(9,64), nn.ReLU(), nn.Linear(64,9))
        self.mlp = nn.Sequential(nn.Linear(9, 16), nn.PReLU(), nn.Linear(16, 32), nn.PReLU(), nn.Linear(32, 9))

    def forward(self, x, target, meta_target):
        x = x.float()
        meta_target = meta_target.float()

        N, _, H, W = x.shape

        answer = x[:, 8:, :, :]
        row3 = x[:, 6:8, :, :]

        row3 = row3.contiguous().view(-1, 160, 160).unsqueeze(1)  # (N, 2, 160, 160) -> (2N, 1, 160, 160)

        row3 = self.maxpool(self.relu(self.bn64(self.conv1(row3))))  # -> (2N, 64, 80, 80)

        answer = answer.contiguous().view(-1, 160, 160).unsqueeze(1) # -> (8N, 1, 160, 160)
        answer = self.maxpool(self.relu(self.bn64(self.conv1(answer)))) # -> (8N, 64, 40, 40)

        answer = answer.view(-1, 8, 64, 40, 40)
        row3 = row3.view(-1, 2, 64, 40, 40)

        final_feature = torch.sum(torch.cat((answer, row3), dim = 1), dim = 1) # -> (N, 10, 64, 80, 80) -> (N, 64, 40, 40)
        final_feature = self.resblock(final_feature)  # -> (N, 64, 20, 20)

        final_feature = self.avgpool(final_feature).view(N, 64) # -> (N, 64, 1, 1) -> (N, 64)

        final_feature = self.summary(final_feature) # -> (N, 8)

        target = torch.zeros(N, 8).cuda().scatter_(1, target.view(-1, 1), 1)
        stu_loss = F.binary_cross_entropy_with_logits(final_feature, target)

        meta_predict = self.mlp_meta(meta_target) # -> (N, 9)
        meta_loss = F.cross_entropy(meta_predict.float(), meta_target.argmax(dim = -1).long())

        return final_feature, stu_loss, meta_loss

class meta(nn.Module):
    def __init__(self):
        super(meta,self).__init__()


    def forward(self, meta, target):
        meta = self.mlp(meta)
        target








