from dataloader import AbstactReasoning
from torch.utils.data import DataLoader
import model
from model import *
import torch.optim as optim
import torch
from tqdm import tqdm
import torch.nn.functional as F
import os
import distrillation
import time
#from resnest.torch import resnest50

def self_softmax(prediction, temperature = None):
    softmax_output = torch.softmax(prediction/float(temperature), dim = 1)
    return softmax_output

#传统蒸馏

class DK(nn.Module):

    def __init__(self, T):
        super(DK, self).__init__()
        self.T = T

    def forward(self, y_s, y_t):
        p_s = F.log_softmax(y_s/self.T, dim=1)
        p_t = F.softmax(y_t/self.T, dim=1)
        loss = F.kl_div(p_s, p_t, size_average=False) * (self.T**2) / y_s.shape[0]
        return loss

# def main():
#     path = '/tmp/pycharm_project_982/data'
#     print(path)
#
#     for name in os.listdir(path):
#
#         print(name)
#         best_accuracy_test = 0.0
#         best_accuracy_train = 0.0
#         best_accuracy_val = 0.0
#         epochs = 0
#
#         title_name = name
#         path_new = os.path.join(path, name)
#
#         train_des = os.path.join(path_new, 'train')
#         test_des = os.path.join(path_new, 'test')
#         val_des = os.path.join(path_new, 'val')
#
#         train_dataset = AbstactReasoning(train_des)
#         valid_dataset = AbstactReasoning(val_des)
#         test_dataset = AbstactReasoning(test_des)
#
#         trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
#         validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
#         testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)
#
#         student = distrillation.student_model()
#         teacher = distrillation.teacher_model()
#
#         temperature = 5
#         student = student.cuda()
#         teacher = teacher.cuda()
#
#         student_optimizer = optim.Adam(student.parameters(), lr = 0.005)
#         #teacher_optimizer = optim.Adam(teacher.parameters(), lr = 0.001)
#         scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)
#
#         best_acc = 0
#         alpha = 0.75
#         beta = 0.25
#         epochs = 50
#         lamda = 0.5
#
#         for epoch in range(epochs):
#
#             student.train()
#             teacher.train()
#
#             for batch_idx, (inputs, target, meta_target) in enumerate(tqdm(trainloader)):
#                 # (N,16,160,160)
#
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#
#                 student_optimizer.zero_grad()
#
#                 stu_pre, stu_loss, stu_meta_loss = student(inputs, target,
#                                                  meta_target)  # soft_prediction = pre ; hard_prediction = pre
#
#                 teacher_pre, teacher_loss = teacher(inputs, target, meta_target)
#
#                 # Cross-Entropy take long tensor target and float tensor input
#
#                 #student_loss = F.cross_entropy(stu_pre.float(), target.argmax(dim=-1).long())
#                 # teacher_loss = 10 * teacher_meta_loss + teacher_pre_loss
#
#                 # loss_total = DK(loss_fn1, loss_fn2)
#                 loss_total = stu_loss + 10 * stu_meta_loss
#                 loss_KD = nn.KLDivLoss()(F.log_softmax(stu_pre / temperature, dim = 1), F.softmax(teacher_pre / temperature, dim = 1))
#
#                 loss_total = (1-lamda) * loss_total + lamda * temperature * temperature * loss_KD
#                 loss_total.backward()
#
#                 # student_loss.backward(retain_graph=True)
#                 # teacher_loss.backward(retain_graph=True)
#                 student_optimizer.step()
#
#             # scheduler.step()
#
#             student.eval()
#
#             with torch.no_grad():
#
#                 trainacc = 0.
#                 step = 0
#                 for batch_idx, (inputs, target, meta_target) in enumerate(trainloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, student_loss, meta_loss = student(inputs, target, meta_target)
#                     pre = pre.argmax(dim=-1)
#                     trainacc += torch.mean((pre == target).float())
#                     step += 1
#                 # print(trainacc)
#                 trainacc /= len(trainloader)
#                 # print(trainacc)
#                 # print(len(trainloader))
#                 print(f'{epoch} epoch train acc {trainacc}')
#
#                 validacc = 0.
#
#                 for batch_idx, (inputs, target, meta_target) in enumerate(validloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, student_loss, meta_loss = student(inputs, target, meta_target)
#                     #   print(inputs.shape)
#                     #   print(target.shape)
#                     pre = pre.argmax(dim=-1)
#                     validacc += torch.mean((pre == target).float())
#
#                 validacc /= len(validloader)
#                 print(f'{epoch} epoch valid acc {validacc}')
#                 if best_acc < validacc:
#                     torch.save(student.state_dict(), 'params.pkl')
#                     best_acc = validacc
#                     print('saving...')
#
#             testacc = 0
#             for batch_idx, (inputs, target, meta_target) in enumerate(testloader):
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#                 pre, student_loss, meta_loss = student(inputs, target, meta_target)
#                 pre = pre.argmax(dim=-1)
#                 testacc += torch.mean((pre == target).float())
#
#             testacc /= len(testloader)
#             print(f'{epoch} epoch best test acc {testacc}')
#
#             if validacc > best_accuracy_val or validacc == best_accuracy_val:
#                 best_accuracy_val = validacc
#                 best_accuracy_train = trainacc
#                 best_accuracy_test = testacc
#                 epochs = epoch + 1
#
#         f = open('/tmp/pycharm_project_982/Results.txt', 'a')
#
#         f.writelines(['\nThe best result of ', str(title_name), ' is shown on epoch: ', str(epochs), ', train: ',
#                       str(best_accuracy_train.item()), ', test: ', str(best_accuracy_test.item()), ', val: ',
#                       str(best_accuracy_val.item()), '\n'])
#         f.close()
def main():
    path = '/tmp/pycharm_project_982/data'

    for name in os.listdir(path):

        print(name)
        best_accuracy_test = 0.0
        best_accuracy_train = 0.0
        best_accuracy_val = 0.0
        epochs = 0

        title_name = name
        path_new = os.path.join(path, name)

        train_des = os.path.join(path_new, 'train')
        test_des = os.path.join(path_new, 'test')
        val_des = os.path.join(path_new, 'val')

        train_dataset = AbstactReasoning(train_des)
        valid_dataset = AbstactReasoning(val_des)
        test_dataset = AbstactReasoning(test_des)

        trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
        validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
        testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)


      #  teacher = model.mainmodel1()

        temperature = 5
        student = model.multipanel()
        student = student.cuda()
       # teacher = teacher.cuda()

      #  optimizer = optim.SGD(net.parameters(), lr=0.08, momentum=0.9, weight_decay=1e-4)
        student_optimizer = optim.Adam(student.parameters(), lr=0.0001)
       # teacher_optimizer = optim.Adam(teacher.parameters(), lr = 0.001)

        scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)

        best_acc = 0.0
        alpha = [0.1, 0.01,0]
        beta = [0.1, 0.01,0]
        epochs = 15
        lamda = 0.5

        for alpha_global in alpha:
            for beta_local in beta:
                if alpha_global == 0 and beta_local == 0:
                    continue
                best_acc = 0.0
                test_acc = 0.0
                train_acc = 0.0
                for epoch in range(epochs):

                    student.train()

                    for batch_idx, (inputs, target, meta_target) in enumerate(tqdm(trainloader)):
                        # (N,16,160,160)

                        inputs, target = inputs.cuda(), target.cuda()
                        meta_target = meta_target.cuda()

                        student_optimizer.zero_grad()
                        #target = torch.zeros(16, 8).cuda().scatter_(1, target.view(-1, 1), 1)

                        global_simi, local_simi, pre = student(inputs, target,
                                                         meta_target)
                        #loss = F.cross_entropy(pre, target)

                        loss = F.nll_loss(pre, target, reduce = False)

                        print(f'the loss value is: {loss.mean()}')
                        print(f'the global_simi value is: {global_simi}')
                        print(f'the local_simi value is: {local_simi}')

                        loss = loss.mean() + alpha_global * global_simi + beta_local * local_simi

                      #  print(torch.autograd.grad(loss, pre))
                        a = list(student.parameters())[0].clone()
                        loss.backward()

                        student_optimizer.step()
                        b = list(student.parameters())[0].clone()
                        print(torch.equal(a.data, b.data))

                    student.eval()

                    with torch.no_grad():

                        trainacc = 0.
                        step = 0
                        for batch_idx, (inputs, target, meta_target) in enumerate(trainloader):
                            inputs, target = inputs.cuda(), target.cuda()
                            meta_target = meta_target.cuda()
                            global_simi, local_simi, pre = student(inputs, target, meta_target)
                            pre = pre.argmax(dim=-1)
                            trainacc += torch.mean((pre == target).float())
                            step += 1

                        trainacc /= len(trainloader)

                        print(f'{epoch} epoch train acc {trainacc}')

                        validacc = 0.

                        for batch_idx, (inputs, target, meta_target) in enumerate(validloader):
                            inputs, target = inputs.cuda(), target.cuda()
                            meta_target = meta_target.cuda()
                            global_simi, local_simi, pre = student(inputs, target, meta_target)
                            #   print(inputs.shape)
                            #   print(target.shape)
                            pre = pre.argmax(dim=-1)
                            validacc += torch.mean((pre == target).float())

                        validacc /= len(validloader)
                        print(f'{epoch} epoch valid acc {validacc}')
                        if best_acc < validacc:
                            torch.save(student.state_dict(), 'params.pkl')
                            best_acc = validacc
                            print('saving...')

                    testacc = 0
                    for batch_idx, (inputs, target, meta_target) in enumerate(testloader):
                        inputs, target = inputs.cuda(), target.cuda()
                        meta_target = meta_target.cuda()
                        global_simi, local_simi, pre = student(inputs, target, meta_target)
                        pre = pre.argmax(dim=-1)
                        testacc += torch.mean((pre == target).float())

                    testacc /= len(testloader)
                    print(f'{epoch} epoch best test acc {testacc}')

                    if validacc == best_acc:
                        test_acc = testacc
                        train_acc = trainacc

                f = open('/tmp/pycharm_project_982/Results.txt', 'a')

                f.writelines(['\n Cos Similarity: The best result of alpha_global: ', str(alpha_global), ' and local_beta: ', str(beta_local),' is shown on epoch: ', str(epochs), ', train: ',
                              str(train_acc.item()), ', test: ', str(test_acc.item()), ', val: ',
                              str(best_acc.item()), '\n'])
                f.close()

# def main():
#     path = '/tmp/pycharm_project_982/data'
#     print(path)
#
#     # get list of models
#     torch.hub.list('zhanghang1989/ResNeSt', force_reload=True)
#
#     # load pretrained models, using ResNeSt-50 as an example
#     # get list of models
#     torch.hub.list('zhanghang1989/ResNeSt', force_reload=True)
#
#     # load pretrained models, using ResNeSt-50 as an example
#     resnest = torch.hub.load('zhanghang1989/ResNeSt', 'resnest50', pretrained=True)
#
#     for name in os.listdir(path):
#
#         print(name)
#         best_accuracy_test = 0.0
#         best_accuracy_train = 0.0
#         best_accuracy_val = 0.0
#         epochs = 0
#
#         title_name = name
#         path_new = os.path.join(path, name)
#
#         train_des = os.path.join(path_new, 'train')
#         test_des = os.path.join(path_new, 'test')
#         val_des = os.path.join(path_new, 'val')
#
#         train_dataset = AbstactReasoning(train_des)
#         valid_dataset = AbstactReasoning(val_des)
#         test_dataset = AbstactReasoning(test_des)
#
#         trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
#         validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
#         testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)
#
#         student = model.studentModel()
#         teacher = model.Teachermodel()
#
#         temperature = 5
#         student = student.cuda()
#         teacher = teacher.cuda()
#
#         student_optimizer = optim.Adam(student.parameters(), lr = 0.0005)
#         teacher_optimizer = optim.Adam(teacher.parameters(), lr = 0.00001)
#
#         scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)
#
#         best_acc = 0
#         alpha = 0.75
#         beta = 0.25
#         epochs = 60
#
#         for epoch in range(epochs):
#
#             student.train()
#             teacher.train()
#             for batch_idx, (inputs, target, meta_target) in enumerate(tqdm(trainloader)):
#                 # (N,16,160,160)
#
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#
#                 student_optimizer.zero_grad()
#                 teacher_optimizer.zero_grad()
#
#                 teacher_pre, teacher_meta_loss = teacher(inputs, target, meta_target)
#
#                 target = torch.zeros(16, 8).cuda().scatter_(1, target.view(-1, 1), 1)
#
#                 # Cross-Entropy take long tensor target and float tensor input
#
#                 teacher_pre_loss = F.cross_entropy(teacher_pre.float(), target.argmax(dim = -1).long())
#                 #teacher_loss = 10 * teacher_meta_loss + teacher_pre_loss
#
#
#
#                 stu_pre, stu_meta_loss = student(inputs, target,
#                                                  meta_target)  # soft_prediction = pre ; hard_prediction = pre
#                 soft_predictions = self_softmax(stu_pre, temperature = temperature)
#                 hard_prediction = self_softmax(stu_pre, temperature = 1)
#                 soft_labels = self_softmax(teacher_pre, temperature = temperature)
#
#                 loss_fn1 = F.cross_entropy(soft_labels.float(),soft_predictions.argmax(dim=-1).long())
#                 loss_fn2 = F.cross_entropy(hard_prediction.float(),target.argmax(dim = -1).long())
#
#
#                 #loss_total = DK(loss_fn1, loss_fn2)
#                 loss_total = loss_fn1 + 10 * loss_fn2 + 10 * stu_meta_loss
#
#                 teacher_pre_loss.backward(retain_graph=True)
#                 loss_total.backward(retain_graph = True)
#
#                 # student_loss.backward(retain_graph=True)
#                 # teacher_loss.backward(retain_graph=True)
#                 teacher_optimizer.step()
#                 student_optimizer.step()
#
#         # scheduler.step()
#
#             student.eval()
#
#             with torch.no_grad():
#
#                 trainacc = 0.
#                 step = 0
#                 for batch_idx, (inputs, target, meta_target) in enumerate(trainloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, loss = student(inputs, target, meta_target)
#                     pre = pre.argmax(dim=-1)
#                     trainacc += torch.mean((pre == target).float())
#                     step += 1
#                 #print(trainacc)
#                 trainacc /= len(trainloader)
#                 #print(trainacc)
#                 #print(len(trainloader))
#                 print(f'{epoch} epoch train acc {trainacc}')
#
#                 validacc = 0.
#
#                 for batch_idx, (inputs, target, meta_target) in enumerate(validloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, loss = student(inputs, target, meta_target)
#                  #   print(inputs.shape)
#                  #   print(target.shape)
#                     pre = pre.argmax(dim=-1)
#                     validacc += torch.mean((pre == target).float())
#
#                 validacc /= len(validloader)
#                 print(f'{epoch} epoch valid acc {validacc}')
#                 if best_acc < validacc:
#                     torch.save(student.state_dict(), 'params.pkl')
#                     best_acc = validacc
#                     print('saving...')
#
#             testacc = 0
#             for batch_idx, (inputs, target, meta_target) in enumerate(testloader):
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#                 pre, loss = student(inputs, target, meta_target)
#                 pre = pre.argmax(dim=-1)
#                 testacc += torch.mean((pre == target).float())
#
#             testacc /= len(testloader)
#             print(f'{epoch} epoch best test acc {testacc}')
#
#             if validacc > best_accuracy_val or validacc == best_accuracy_val:
#                 best_accuracy_val = validacc
#                 best_accuracy_train = trainacc
#                 best_accuracy_test = testacc
#                 epochs = epoch + 1
#
#
#         f = open('/tmp/pycharm_project_982/Results.txt', 'a')
#
#         f.writelines(['\nThe best result of ', str(title_name), ' is shown on epoch: ', str(epochs), ', train: ',
#                       str(best_accuracy_train.item()), ', test: ', str(best_accuracy_test.item()), ', val: ',
#                       str(best_accuracy_val.item()), '\n'])
#         f.close()

#         torch.cuda.empty_cache()


# def main():
#     path = '/tmp/pycharm_project_982/data'
#     print(path)
#
#     for name in os.listdir(path):
#
#         print(name)
#         best_accuracy_test = 0.0
#         best_accuracy_train = 0.0
#         best_accuracy_val = 0.0
#         epochs = 0
#
#         title_name = name
#         path_new = os.path.join(path, name)
#
#         train_des = os.path.join(path_new, 'train')
#         test_des = os.path.join(path_new, 'test')
#         val_des = os.path.join(path_new, 'val')
#
#         train_dataset = AbstactReasoning(train_des)
#         valid_dataset = AbstactReasoning(val_des)
#         test_dataset = AbstactReasoning(test_des)
#
#         #
#         # train_dataset = AbstactReasoning(distribute_nine)
#         # valid_dataset = AbstactReasoning('/tmp/pycharm_project_572/data/center_single/val')
#         # test_dataset = AbstactReasoning('/tmp/pycharm_project_572/data/center_single/test')
#
#         trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
#         validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
#         testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)
#
#         student = model.mainmodel1()
#    #     teacher = model.generalization()
#
#         temperature = 5
#         student = student.cuda()
#    #     teacher = teacher.cuda()
#
#         student_optimizer = optim.Adam(student.parameters(), lr=0.05)
#    #     teacher_optimizer = optim.SGD(teacher.parameters(), lr=0.01, momentum=0.9, weight_decay=1e-4)
#
#         scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)
#
#         best_acc = 0
#         alpha = 0.75
#         beta = 0.25
#         epochs = 40
#
#         for epoch in range(epochs):
#
#             student.train()
#    #         teacher.train()
#             for batch_idx, (inputs, target, meta_target) in enumerate(tqdm(trainloader)):
#                 # (N,16,160,160)
#
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#                 pre,loss = student(inputs, target, meta_target)
#    #             teacher_pre = teacher(inputs, target)
#
#                 # teacher_pre = torch.LongTensor(teacher_pre)
#                 # stu_pre = torch.LongTensor(stu_pre)
#     #            teacher_loss = F.cross_entropy(self_softmax(teacher_pre, temperature=temperature),
#     #                                           teacher_pre.argmax(dim=-1))
#     #            student_loss1 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature), stu_pre.argmax(dim=-1))
#     #            student_loss2 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature),
#     #                                            teacher_pre.argmax(dim=-1))
#
#      #       student_loss = alpha * student_loss1 + beta * student_loss2
#
#                 student_optimizer.zero_grad()
#                 loss.backward()
#      #       teacher_optimizer.zero_grad()
#      #       student_loss.backward(retain_graph=True)
#      #       teacher_loss.backward(retain_graph=True)
#                 student_optimizer.step()
#      #       teacher_optimizer.step()
#         # scheduler.step()
#
#             student.eval()
#
#             with torch.no_grad():
#
#                 trainacc = 0.
#                 step = 0
#                 for batch_idx, (inputs, target, meta_target) in enumerate(trainloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, loss = student(inputs, target, meta_target)
#                     pre = pre.argmax(dim=-1)
#                     trainacc += torch.mean((pre == target).float())
#                     step += 1
#                 #print(trainacc)
#                 trainacc /= len(trainloader)
#                 #print(trainacc)
#                 #print(len(trainloader))
#                 print(f'{epoch} epoch train acc {trainacc}')
#
#                 validacc = 0.
#
#                 for batch_idx, (inputs, target, meta_target) in enumerate(validloader):
#                     inputs, target = inputs.cuda(), target.cuda()
#                     meta_target = meta_target.cuda()
#                     pre, loss = student(inputs, target, meta_target)
#                  #   print(inputs.shape)
#                  #   print(target.shape)
#                     pre = pre.argmax(dim=-1)
#                     validacc += torch.mean((pre == target).float())
#
#                 validacc /= len(validloader)
#                 print(f'{epoch} epoch valid acc {validacc}')
#                 if best_acc < validacc:
#                     torch.save(student.state_dict(), 'params.pkl')
#                     best_acc = validacc
#                     print('saving...')
#
#             testacc = 0
#             for batch_idx, (inputs, target, meta_target) in enumerate(testloader):
#                 inputs, target = inputs.cuda(), target.cuda()
#                 meta_target = meta_target.cuda()
#                 pre, loss = student(inputs, target, meta_target)
#                 pre = pre.argmax(dim=-1)
#                 testacc += torch.mean((pre == target).float())
#
#             testacc /= len(testloader)
#             print(f'{epoch} epoch best test acc {testacc}')
#
#             if validacc > best_accuracy_val or validacc == best_accuracy_val:
#                 best_accuracy_val = validacc
#                 best_accuracy_train = trainacc
#                 best_accuracy_test = testacc
#                 epochs = epoch + 1
#
#
#         f = open('/tmp/pycharm_project_982/Results.txt', 'a')
#
#         f.writelines(['\nThe best result of ', str(title_name), ' is shown on epoch: ', str(epochs), ', train: ',
#                       str(best_accuracy_train.item()), ', test: ', str(best_accuracy_test.item()), ', val: ',
#                       str(best_accuracy_val.item()), '\n'])
#         f.close()
#
#         torch.cuda.empty_cache()


# 0.25+0.75, 0.5+0.5, 0.75+0.25
# dropout = 0.3, 0.5
#temperature = 1 3 5 10
# gene+main
#
# def main():
#
#     droprate = 0.3
#     temperature = [1, 3, 5, 10]
#     gene = [0, 0.25, 0.5, 0.75, 1]
#     normal = [0, 0.25, 0.5, 0.75, 1]
#     path = '/tmp/pycharm_project_982/data/distribute_nine'
#
#     train_des = os.path.join(path, 'train')
#     test_des = os.path.join(path, 'test')
#     val_des = os.path.join(path, 'val')
#
#     train_dataset = AbstactReasoning(train_des)
#     valid_dataset = AbstactReasoning(val_des)
#     test_dataset = AbstactReasoning(test_des)
#
#     trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
#     validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
#     testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)
#
#     for temp in temperature:
#         for fanhua in gene:
#             for putong in normal:
#
#                 if fanhua + putong != 1:
#                     continue
#                 else:
#                     temperature = temp
#                     best_accuracy_test = 0.0
#                     best_accuracy_train = 0.0
#                     best_accuracy_val = 0.0
#                     epochs = 0
#
#                     student = model.generalization()
#                     teacher = model.mainmodel1()
#
#                     student = student.cuda()
#                     teacher = teacher.cuda()
#
#                     student_optimizer = optim.SGD(student.parameters(), lr=0.08, momentum=0.9, weight_decay=1e-4)
#                     teacher_optimizer = optim.SGD(teacher.parameters(), lr=0.01, momentum=0.9, weight_decay=1e-4)
#
#                     scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)
#
#                     best_acc = 0
#
#                     for epoch in range(50):
#
#                         student.train()
#                         teacher.train()
#                         for batch_idx, (inputs, target) in enumerate(tqdm(trainloader)):
#                             # (N,16,160,160)
#
#                             inputs, target = inputs.cuda(), target.cuda()
#                             stu_pre = student(inputs, target)
#                             teacher_pre = teacher(inputs, target)
#
#                             # teacher_pre = torch.LongTensor(teacher_pre)
#                             # stu_pre = torch.LongTensor(stu_pre)
#                             teacher_loss = F.cross_entropy(self_softmax(teacher_pre, temperature=temperature),
#                                                            teacher_pre.argmax(dim=-1))
#                             student_loss1 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature),
#                                                             stu_pre.argmax(dim=-1))
#                             student_loss2 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature),
#                                                             teacher_pre.argmax(dim=-1))
#
#                             student_loss = fanhua * student_loss1 + putong * student_loss2
#
#                             student_optimizer.zero_grad()
#                             teacher_optimizer.zero_grad()
#                             student_loss.backward(retain_graph=True)
#                             teacher_loss.backward(retain_graph=True)
#                             student_optimizer.step()
#                             teacher_optimizer.step()
#                             # scheduler.step()
#
    #                         student.eval()
#     #
#     #                         with torch.no_grad():
#     #
#     #                             trainacc = 0.
#     #                             step = 0
#     #                             for batch_idx, (inputs, target) in enumerate(trainloader):
#     #                                 inputs, target = inputs.cuda(), target.cuda()
#     #                                 pre = student(inputs, target)
#     #                                 if epoch % 10 == 0 and step == 3:
#     #                                     pass
#     #                                     # print(f'pre is {pre[:5]}, loss {loss}')
#     #                                     # print(f'loss is {loss}')
#     #                                 pre = pre.argmax(dim=-1)
#     #                                 trainacc += torch.mean((pre == target).float())
#     #                                 step += 1
#     #
#     #                             trainacc /= len(trainloader)
#     #                             print(f'{epoch} epoch train acc {trainacc}')
#     #
#     #                             validacc = 0.
#     #                             for batch_idx, (inputs, target) in enumerate(validloader):
#     #                                 inputs, target = inputs.cuda(), target.cuda()
#     #                                 pre = student(inputs, target)
#     #                                 pre = pre.argmax(dim=-1)
#     #                                 validacc += torch.mean((pre == target).float())
#     #
#     #                             validacc /= len(validloader)
#     #                             print(f'{epoch} epoch valid acc {validacc}')
#     #                             if best_acc > validacc:
#     #                                 torch.save(student.state_dict(), 'params.pkl')
#     #                                 best_acc = validacc
#     #
#     #                             testacc = 0
#     #                             for batch_idx, (inputs, target) in enumerate(testloader):
#     #                                 inputs, target = inputs.cuda(), target.cuda()
#     #                                 pre = student(inputs, target)
#     #                                 pre = pre.argmax(dim=-1)
#     #                                 testacc += torch.mean((pre == target).float())
#     #
#     #                             testacc /= len(testloader)
#     #                             print(f'{epoch} epoch test acc {testacc}')
#     #
#     #                             if validacc > best_accuracy_val or validacc == best_accuracy_val:
#     #                                 best_accuracy_val = validacc
#     #                                 best_accuracy_train = trainacc
#     #                                 best_accuracy_test = testacc
#     #                                 epochs = epoch + 1
#     #
#     #                     f = open('/tmp/pycharm_project_982/Results.txt', 'a')
#     #
#     #                     f.writelines(
#     #                         ['\n\nThe best result of distribute_nine is shown on epoch: ', str(epochs), ', train: ',
#     #                          str(best_accuracy_train.item()), ', test: ', str(best_accuracy_test.item()), ', val: ',
#     #                          str(best_accuracy_val.item()), '\n','with temperate: ', str(temp), ', and generalization rate: ', str(fanhua), ' , and normal rate: ', str(putong)])
#     #                     f.close()
#     #
#     #                     torch.cuda.empty_cache()


# def main():
#     path = '/tmp/pycharm_project_982/data'
#     print(path)
#
#     for name in os.listdir(path):
#
#         print(name)
#         best_accuracy_test = 0.0
#         best_accuracy_train = 0.0
#         best_accuracy_val = 0.0
#         epochs = 0
#
#         title_name = name
#         path_new = os.path.join(path, name)
#
#         train_des = os.path.join(path_new, 'train')
#         test_des = os.path.join(path_new, 'test')
#         val_des = os.path.join(path_new, 'val')
#
#         train_dataset = AbstactReasoning(train_des)
#         valid_dataset = AbstactReasoning(val_des)
#         test_dataset = AbstactReasoning(test_des)
#
#         #
#         # train_dataset = AbstactReasoning(distribute_nine)
#         # valid_dataset = AbstactReasoning('/tmp/pycharm_project_572/data/center_single/val')
#         # test_dataset = AbstactReasoning('/tmp/pycharm_project_572/data/center_single/test')
#
#         trainloader = DataLoader(train_dataset, batch_size=16, shuffle=True)
#         validloader = DataLoader(valid_dataset, batch_size=16, shuffle=False)
#         testloader = DataLoader(test_dataset, batch_size=16, shuffle=False)
#
#         student = model.generalization()
#         teacher = model.generalization()
#
#         temperature = 5
#         student = student.cuda()
#         teacher = teacher.cuda()
#
#         student_optimizer = optim.SGD(student.parameters(), lr=0.08, momentum=0.9, weight_decay=1e-4)
#         teacher_optimizer = optim.SGD(teacher.parameters(), lr=0.01, momentum=0.9, weight_decay=1e-4)
#
#         scheduler = optim.lr_scheduler.MultiStepLR(student_optimizer, milestones=[30, 80], gamma=0.1)
#
#         best_acc = 0
#         alpha = 0.75
#         beta = 0.25
#
#         for epoch in range(50):
#
#             student.train()
#             teacher.train()
#             for batch_idx, (inputs, target) in enumerate(tqdm(trainloader)):
#                 # (N,16,160,160)
#
#                 inputs, target = inputs.cuda(), target.cuda()
#                 stu_pre = student(inputs, target)
#                 teacher_pre = teacher(inputs, target)
#
#                 # teacher_pre = torch.LongTensor(teacher_pre)
#                 # stu_pre = torch.LongTensor(stu_pre)
#                 teacher_loss = F.cross_entropy(self_softmax(teacher_pre, temperature=temperature),
#                                                teacher_pre.argmax(dim=-1))
#                 student_loss1 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature), stu_pre.argmax(dim=-1))
#                 student_loss2 = F.cross_entropy(self_softmax(stu_pre, temperature=temperature),
#                                                 teacher_pre.argmax(dim=-1))
#
#             student_loss = alpha * student_loss1 + beta * student_loss2
#
#             student_optimizer.zero_grad()
#             teacher_optimizer.zero_grad()
#             student_loss.backward(retain_graph=True)
#             teacher_loss.backward(retain_graph=True)
#             student_optimizer.step()
#             teacher_optimizer.step()
#         # scheduler.step()
#
    #         student.eval()
    #
    #         with torch.no_grad():
    #
    #             trainacc = 0.
    #             step = 0
    #             for batch_idx, (inputs, target) in enumerate(trainloader):
    #                 inputs, target = inputs.cuda(), target.cuda()
    #                 pre = student(inputs, target)
    #                 if epoch % 10 == 0 and step == 3:
    #                     pass
    #                 # print(f'pre is {pre[:5]}, loss {loss}')
    #                 # print(f'loss is {loss}')
    #                 pre = pre.argmax(dim=-1)
    #                 trainacc += torch.mean((pre == target).float())
    #                 step += 1
    #
    #             trainacc /= len(trainloader)
    #             print(f'{epoch} epoch train acc {trainacc}')
    #
    #             validacc = 0.
    #             for batch_idx, (inputs, target) in enumerate(validloader):
    #                 inputs, target = inputs.cuda(), target.cuda()
    #                 pre = student(inputs, target)
    #                 pre = pre.argmax(dim=-1)
    #                 validacc += torch.mean((pre == target).float())
    #
    #             validacc /= len(validloader)
    #             print(f'{epoch} epoch valid acc {validacc}')
    #             if best_acc > validacc:
    #                 torch.save(student.state_dict(), 'params.pkl')
    #                 best_acc = validacc
    #
    #             testacc = 0
    #             for batch_idx, (inputs, target) in enumerate(testloader):
    #                 inputs, target = inputs.cuda(), target.cuda()
    #                 pre = student(inputs, target)
    #                 pre = pre.argmax(dim=-1)
    #                 testacc += torch.mean((pre == target).float())
    #
    #             testacc /= len(testloader)
    #             print(f'{epoch} epoch test acc {testacc}')
    #
    #             if validacc > best_accuracy_val or validacc == best_accuracy_val:
    #                 best_accuracy_val = validacc
    #                 best_accuracy_train = trainacc
    #                 best_accuracy_test = testacc
    #                 epochs = epoch + 1
    #
    #     f = open('/tmp/pycharm_project_982/Results.txt', 'a')
    #
    #     f.writelines(['\nThe best result of ', str(title_name), ' is shown on epoch: ', str(epochs), ', train: ',
    #                   str(best_accuracy_train.item()), ', test: ', str(best_accuracy_test.item()), ', val: ',
    #                   str(best_accuracy_val.item()), '\n'])
    #     f.close()
#
#     torch.cuda.empty_cache()



if __name__ == '__main__':
    main()