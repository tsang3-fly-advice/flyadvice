import argparse
import dataloader
import model_file
from tqdm import tqdm
import torch.nn as nn
import torch
import random
import numpy as np
import pickle
import torch.nn.functional as F

def get_args():

    arg = argparse.ArgumentParser()
    # model parameter
    arg.add_argument('--seed', default=2019, type=int)
    arg.add_argument('--k1',default=80, type=int) # filter size
    arg.add_argument('--k2', default=32 , type=int) #ID embedding dim
    arg.add_argument('--t', default=32, type=int) #hidden size of attention network
    arg.add_argument('--word_embedding' , default=300,type=int) #word_embedding
    arg.add_argument('--n', default= 20, type=int) # final dim of representation
    arg.add_argument('--num_filters', default=100, type=int) #number of filters per filter size
    arg.add_argument('--dropout', default=0, type=int)
    arg.add_argument('--gpu_id', default=0, type=int)
    # ANR
    arg.add_argument('--K', default=5, type=int) # Number of Aspects
    arg.add_argument('--h1',default=10,type=int) #Dimensionality of the Aspect-level Representations
    arg.add_argument('--h2', default=50, type=int) #Dimensionality of the Hidden Layers used for Aspect Importance Estimation

    #D_ATT
    arg.add_argument('--latt', default=200, type=int)
    arg.add_argument('--gatt', default=100, type=int)
    #arg.add_argument('--input_size', default=10000, type=int)

    #NeuMF & FM
    arg.add_argument("--factor_num", type=int,default=32, help="predictive factors numbers in the model")
    arg.add_argument("--num_layers", type=int,default=3, help="number of layers in MLP model")

    # train parameter
    arg.add_argument('--batch_size', default=32, type=int)
    arg.add_argument('--lr', default= 4e-3, type=float)

    arg.add_argument('--model', default='DeepConn_Gumbel', type=str)

    # 原始的 -> Gumbel -> Poisson -> GMM -> Weibull -> Frechet

    #可选:NARRE,NARRE_Gumbel,ANR,ANR_Gumbel,DeepConn,DeepConn_Gumbel,D_Att,D_Att_Gumbel,_Gumbel,PMF,PMF_Gumbel,,NCF_Gumbel

    arg.add_argument('--epoch', default=10,type=int)
    arg.add_argument('--device', default=0, type=int)

    arg.add_argument('--step_size', default=2, type=int)
    arg.add_argument('--gamma', default=0.8, type=int)

    return arg.parse_args()


if __name__ == '__main__':

    data_list = ['Office_Products_5']
    model_list = ['NRPA_Weibull']
#'DeepConn_Gumbel', 'PMF_Gumbel', 'NARRE_Gumbei', 'NRPA_Gumbel', 'NCF_Gumbel'
    #,'Toys_and_Games_5',"Musical_Instruments_5",'Grocery_and_Gourmet_Food_5','Tools_and_Home_Improvement_5'
                # ,'Office_Products_5','Patio_Lawn_and_Garden_5','Automotive_5']#['Patio_Lawn_and_Garden_5',"Musical_Instruments_5",'Automotive_5','Office_Products_5','Digital_Music_5','Grocery_and_Gourmet_Food_5','Toys_and_Games_5','Tools_and_Home_Improvement_5'] #[]
                #['Grocery_and_Gourmet_Food_5','Automotive_5','Tools_and_Home_Improvement_5',"Musical_Instruments_5"]
               # "Musical_Instruments_5",'Tools_and_Home_Improvement_5','Grocery_and_Gourmet_Food_5',]
                #'Patio_Lawn_and_Garden_5',"Musical_Instruments_5",'Tools_and_Home_Improvement_5','Grocery_and_Gourmet_Food_5','Toys_and_Games_5','Digital_Music_5','Automotive_5']

                #'Automotive_5','Instant_Video_5', 'Office_Products_5','Patio_Lawn_and_Garden_5',\'Instant_Video_5','Automotive_5'
                #'Grocery_and_Gourmet_Food_5','Digital_Music_5',"Musical_Instruments_5",'Kindle_Store_5'] "Musical_Instruments_5"
                #'Tools_and_Home_Improvement_5','Grocery_and_Gourmet_Food_5',,'Toys_and_Games_5','Digital_Music_5','Automotive_5'

    print("Model by the Minimum value")
    print()
    for data_name in data_list:
        for model_name in model_list:

            if data_name == 'Grocery_and_Gourmet_Food_5' and model_name == 'PMF_Wisbull':
                continue
            elif data_name == 'Grocery_and_Gourmet_Food_5' and model_name == 'PMF_Frechet':
                continue

            args = get_args()
            batch_size = args.batch_size
            learning_rate = args.lr
            args.model = model_name

            torch.manual_seed(args.seed)
            torch.cuda.manual_seed_all(args.seed)
            torch.cuda.manual_seed(args.seed)
            np.random.seed(args.seed)
            random.seed(args.seed)
            torch.manual_seed(args.seed)
            torch.backends.cudnn.deterministic = True
            print(f'lr:{args.lr},step:{args.step_size},gamma:{args.gamma}')

        #若以下文件不在本文件夹,将其替换为文件所在路径
            vocab_dict_path = '/home/wyx/gumbel/glove.6B.300d.txt' #f'./glove.6B.300d.txt'
            file_path = '/home/wyx/gumbel/data/' + data_name+ '.json'
            # f'./{data_name}.json'
            glove_data = 'data/' +data_name +'_.glove_data.pkl'
            glove_matrix = 'data/' + data_name + '_glove_matrix.pkl'

            glove_data, matrix, review_len = dataloader.word_to_id(glove_data,glove_matrix,vocab_dict_path,file_path)
            train_data,test_data,user_dict,item_dict,u_max,i_max, num_users,num_items = dataloader.prepare_data(glove_data)
            batch = dataloader.Batch(train_data, test_data,user_dict,item_dict, u_max, i_max, batch_size, review_len,train=True) #(review_len是一条评论的长度)

            if args.model == 'NARRE' or args.model == 'NARRE_Gumbel' or args.model == 'NARRE_Poisson'or args.model == 'NARRE_GMM' or args.model == 'NARRE_Weibull' or args.model == 'NARRE_Frechet' or args.model == 'NARRE_Expon':
                mainmodel = model_file.NARRE(num_users,num_items,matrix,review_len,args)

            elif args.model == 'PMF_Gumbel' or args.model == 'PMF_Poisson'or args.model == 'PMF_GMM' or args.model == 'PMF_Weibull' or args.model == 'PMF_Frechet':
                mainmodel = model_file.PMF_Gumbel(num_users,num_items,args)

            elif args.model == 'PMF':
                mainmodel = model_file.PMF(num_users,num_items)

            elif args.model == 'ANR' or args.model == 'ANR_Gumbel':
                mainmodel = model_file.ANR(num_users,num_items,matrix, review_len, args)

            elif args.model == 'D_Att' or args.model == 'D_Att_Gumbel' or args.model == 'D_Att_Poisson':
                mainmodel = model_file.D_Att(matrix,num_users,num_items,review_len,args)

            elif args.model == 'DeepConn' or args.model == 'DeepConn_Gumbel'or args.model == 'DeepConn_GMM' or args.model == 'DeepConn_Poisson' or args.model == 'DeepConn_Weibull' or args.model == 'DeepConn_Frechet' or args.model == 'DeepConn_Expon':
                mainmodel = model_file.DeepConn(matrix, num_users, num_items, review_len, args)

            elif args.model == 'PMF_Gumbel'or args.model == 'PMF_GMM'or args.model == 'PMF_Poisson' or args.model == 'PMF_Frechet' or args.model == 'PMF_Expon':
                mainmodel = model_file.PMF_Gumbel(num_users,num_items,args)

            elif args.model == 'NCF' or args.model == 'NCF_Gumbel'or args.model == 'NCF_GMM' or args.model == 'NCF_Poisson' or args.model == 'NCF_Frechet':
                mainmodel = model_file.NCF(num_users,num_items,args)

            elif args.model == 'NRPA'or args.model == 'NRPA_Gumbel' or args.model == 'NRPA_Poisson' or args.model == 'NRPA_GMM' or args.model == 'NRPA_Frechet' or args.model == 'NRPA_Weibull':
                mainmodel = model_file.NRPA(num_users,num_items,matrix, review_len, args)

            elif args.model == 'FM' or args.model == 'FM_Gumbel':
                mainmodel = model_file.FM(num_users,num_items,args)


            mainmodel = mainmodel.cuda(args.device)
            loss_func = nn.MSELoss()
            optimizer = torch.optim.Adam(mainmodel.parameters(), learning_rate, weight_decay=0)
            scheduler = torch.optim.lr_scheduler.StepLR(optimizer,step_size=args.step_size,gamma=args.gamma)
            print(f'Model: {args.model}, Dataset: {data_name}')
            best_mse = 999
            improve = 0
            #all_score = []

            for epoch in range(args.epoch):
                batch.set_train(True)
                mainmodel.train()
                for encode_vectors, user_review_lists, uitem_id_list, item_review_lists,\
                    iuser_id_list, overalls, user_id,item_id,u_rating_ratio,i_rating_ratio,mean,var in batch:

                    #all_score += overalls.tolist()

                    user_review_lists = user_review_lists.cuda(args.device)
                    user_id = user_id.cuda(args.device)
                    item_review_lists = item_review_lists.cuda(args.device)
                    item_id = item_id.cuda(args.device)
                    overalls = overalls.cuda(args.device)
                    u_rating_ratio = u_rating_ratio.cuda(args.device)
                    i_rating_ratio = i_rating_ratio.cuda(args.device)
                    uitem_id_list = uitem_id_list.cuda(args.device)
                    iuser_id_list = iuser_id_list.cuda(args.device)
                    mean = mean.cuda(args.device)
                    var = var.cuda(args.device)

                    for p in mainmodel.parameters():
                        p.requires_grad = True

                    optimizer.zero_grad()

                    pre = mainmodel(user_id,item_id,user_review_lists,item_review_lists,u_rating_ratio,i_rating_ratio,args,uitem_id_list,iuser_id_list)
                    #loss = loss_func(pre, overalls)
                    loss = F.mse_loss(pre,overalls)
                    loss.backward()
                    optimizer.step()
                    scheduler.step(epoch)

                batch.set_train(False)
                mainmodel.eval()
                #mainmodel.load_state_dict(torch.load(f'{args.model}_params.pkl'))
                total_loss = 0
                num = 0
                for encode_vectors, user_review_lists, uitem_id_list, item_review_lists, iuser_id_list,\
                    overalls, user_id, item_id, u_rating_ratio,i_rating_ratio, mean, var in batch:

                    user_review_lists = user_review_lists.cuda(args.device)
                    user_id = user_id.cuda(args.device)
                    item_review_lists = item_review_lists.cuda(args.device)
                    item_id = item_id.cuda(args.device)
                    overalls = overalls.cuda(args.device)
                    u_rating_ratio = u_rating_ratio.cuda(args.device)
                    i_rating_ratio = i_rating_ratio.cuda(args.device)
                    uitem_id_list = uitem_id_list.cuda(args.device)
                    iuser_id_list = iuser_id_list.cuda(args.device)
                    mean = mean.cuda(args.device)
                    var = var.cuda(args.device)

                    for p in mainmodel.parameters():
                        p.requires_grad = False

                    pre = mainmodel(user_id,item_id,user_review_lists,item_review_lists,u_rating_ratio,i_rating_ratio,args,uitem_id_list,iuser_id_list)
                    loss = torch.sum(torch.abs(pre-overalls))
                    #loss = F.mse_loss(pre,overalls)
                    total_loss += loss
                    num += len(pre)
                    #num += 1

                total_loss /= num
                if total_loss < best_mse:
                    best_mse = total_loss
                    torch.save(mainmodel.state_dict(),f'{args.model}_params.pkl')
                print(f'epoch {epoch} test loss {total_loss}')
                torch.cuda.empty_cache()
                print()
        '''
        mainmodel.load_state_dict(torch.load(f'{args.model}_params.pkl'))
        batch.set_train(False)
        mainmodel.eval()
        real_score = [[],[],[],[],[]]
        best_mae = 0
        num =0
        score_num = [0, 0, 0, 0, 0]
        step = 0
        for encode_vectors, user_review_lists, uitem_id_list, item_review_lists, iuser_id_list, \
            overalls, user_id, item_id, u_rating_ratio, i_rating_ratio, mean, var in tqdm(batch):
            user_review_lists = user_review_lists.cuda(args.device)
            user_id = user_id.cuda(args.device)
            item_review_lists = item_review_lists.cuda(args.device)
            item_id = item_id.cuda(args.device)
            overalls = overalls.cuda(args.device)
            u_rating_ratio = u_rating_ratio.cuda(args.device)
            i_rating_ratio = i_rating_ratio.cuda(args.device)
            uitem_id_list = uitem_id_list.cuda(args.device)
            iuser_id_list = iuser_id_list.cuda(args.device)

            for p in mainmodel.parameters():
                p.requires_grad = False

            pre = mainmodel(user_id, item_id, user_review_lists, item_review_lists, u_rating_ratio, i_rating_ratio,args, uitem_id_list, iuser_id_list)
            best_mae += torch.sum(torch.abs(pre-overalls))
            num += len(overalls)
            #pre = pre.tolist()
            #overalls = overalls.tolist()
            for i in range(1,6):
                mask = overalls==i
                score_num[i-1] += float(torch.sum(mask).cpu())
                a = torch.masked_select(pre, mask.cuda()).cpu().tolist()
                real_score[i-1] += a

        with open(f'data/{args.model}_{data_name}_result.pkl','wb') as f:
            pickle.dump(real_score,f)

        print(f'{data_name} best test mae {best_mae/num}')
        print(f'score num {score_num}')
        '''
