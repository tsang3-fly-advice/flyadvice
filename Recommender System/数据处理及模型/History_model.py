import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable

class Teachermodel(nn.Module):
    def __init__(self):
        super(Teachermodel,self).__init__()

        self.stu_conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.stu_batch1 = nn.BatchNorm2d(64)

        self.stu_res1 = ResBlock(64, 128, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(128)))

        self.stu_conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.stu_batch2 = nn.BatchNorm2d(64)

        self.stu_avgpool = nn.AdaptiveAvgPool2d((1, 1))

        self.stu_res2 = ResBlock(128, 256, stride=2, downsample=nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(256)))

        self.stu_res4 = ResBlock(256, 64, stride=2, downsample=nn.Sequential(
            nn.Conv2d(256, 64, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(64)))


        self.summarize = nn.Sequential(nn.Linear(64, 64), nn.ReLU(), nn.Linear(64, 8))
        self.ReLU = nn.ReLU()
        self.meta_mlp = nn.Sequential(nn.Linear(9, 16), nn.ReLU(), nn.Linear(16, 8))

    def forward(self, x, target, meta_target):

        x = x.float()
        meta_target = meta_target.float()
# batch, channel, height, width
        input_features = x[:, :8, :, :] # (N, 8, 160, 160)
        input_features = input_features.contiguous().view(-1, 160, 160).unsqueeze(1) #(8*N, 1, 160, 160)
        temp = self.stu_batch1(self.ReLU(self.stu_conv1(input_features)))  # (8*N, 64, 80, 80)
        temp = temp.view(-1, 8, 64, 80, 80)

        row1_features = torch.sum(temp[:, 0:3, :, :, :], dim = 1) # (N, 64, 80, 80)
        row2_features = torch.sum(temp[:, 3:6, :, :, :], dim = 1) # (N, 64, 80, 80)
        row3_features = temp[:, 6:8, :, :, :] # (N, 2, 64, 80, 80)

        #row3_features = row3_features.view(-1, 2, 64, 80, 80)

        row1_features = self.stu_res1(row1_features) # (N, 128, 40, 40)

        row2_features = self.stu_res1(row2_features) # (N, 128, 40, 40)

        # column1_features = torch.sum(torch.index_select(input_features,1, torch.LongTensor([0,3,6])), dim = 1) # (N, 64, 40, 40)
        # column2_features =  torch.sum(torch.index_select(input_features, 1, torch.LongTensor([1,4,7])), dim = 1) # (N, 64, 40, 40)

        answers = x[:, 8:, :, :] # (N, 8, 160, 160)
        answers = answers.contiguous().view(-1, 160, 160).unsqueeze(1) #(8*N, 1, 160, 160)

        answers = self.stu_batch1(self.ReLU(self.stu_conv1(answers))) # (8*N, 64, 80, 80)   Conv -> Relu -> BN
        answers = answers.view(-1, 8, 64, 80, 80) #(N, 8, 64, 40, 40)
        row3_features = torch.sum(torch.cat((answers,row3_features), dim = 1), dim = 1) # -> (N, 10, 64, 80, 80) -> (N, 64, 80, 80)
        row3_features = self.stu_res1(row3_features) # (N, 128, 40, 40)

        row1_features = row1_features.unsqueeze(1)
        row2_features = row2_features.unsqueeze(1)

        row3_features = row3_features.unsqueeze(1) # (N, 1, 128, 40, 40)

        choice = torch.sum(torch.cat((row1_features, row2_features, row3_features), dim = 1), dim = 1)
        choice = self.stu_res2(choice) # (N, 256, 20, 20)
        choice = self.stu_res4(choice) # (N, 64, 10, 10)

        choice = self.stu_avgpool(choice).view(-1, 64) # -> (N, 64)

        final = self.summarize(choice) # (N, 8)

        # target = torch.zeros(16, 8).cuda().scatter_(1, target.view(-1, 1), 1)
        # loss = F.binary_cross_entropy_with_logits(final, target)

        meta = self.meta_mlp(meta_target)
        meta_loss = F.binary_cross_entropy_with_logits(final, meta)

        return final,meta_loss

class studentModel(nn.Module):
    def __init__(self):
        super(studentModel,self).__init__()

        self.stu_conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.stu_batch1 = nn.BatchNorm2d(64)

        self.stu_res1 = ResBlock(64, 128, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(128)))

        self.stu_conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.stu_batch2 = nn.BatchNorm2d(64)
        self.stu_summary = nn.Sequential(nn.Linear(128, 256), nn.ReLU(), nn.Linear(256,1))
        self.stu_avgpool = nn.AdaptiveAvgPool2d((1, 1))

        self.stu_res2 = ResBlock(128, 256, stride=2, downsample=nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(256)))

        self.summarize = nn.Sequential(nn.Linear(256, 256), nn.ReLU(), nn.Linear(256, 8))
        self.ReLU = nn.ReLU()
        self.meta_mlp = nn.Sequential(nn.Linear(9, 16), nn.ReLU(), nn.Linear(16, 8))

    def forward(self, x, target, meta_target):

        x = x.float()
        meta_target = meta_target.float()
# batch, channel, height, width
        input_features = x[:, :8, :, :] # (N, 8, 160, 160)
        input_features = input_features.contiguous().view(-1, 160, 160).unsqueeze(1) #(8*N, 1, 160, 160)
        temp = self.stu_batch1(self.ReLU(self.stu_conv1(input_features)))  # (8*N, 64, 80, 80)
        temp = temp.view(-1, 8, 64, 80, 80)

        row1_features = torch.sum(temp[:, 0:3, :, :, :], dim = 1) # (N, 64, 80, 80)
        row2_features = torch.sum(temp[:, 3:6, :, :, :], dim = 1) # (N, 64, 80, 80)
        row3_features = temp[:, 6:8, :, :, :] # (N, 2, 64, 80, 80)

        #row3_features = row3_features.view(-1, 2, 64, 80, 80)

        row1_features = self.stu_res1(row1_features) # (N, 128, 40, 40)
        row2_features = self.stu_res1(row2_features) # (N, 128, 40, 40)

        # column1_features = torch.sum(torch.index_select(input_features,1, torch.LongTensor([0,3,6])), dim = 1) # (N, 64, 40, 40)
        # column2_features =  torch.sum(torch.index_select(input_features, 1, torch.LongTensor([1,4,7])), dim = 1) # (N, 64, 40, 40)

        answers = x[:, 8:, :, :] # (N, 8, 160, 160)
        answers = answers.contiguous().view(-1, 160, 160).unsqueeze(1) #(8*N, 1, 160, 160)

        answers = self.stu_batch1(self.ReLU(self.stu_conv1(answers))) # (8*N, 64, 80, 80)   Conv -> Relu -> BN
        answers = answers.view(-1, 8, 64, 80, 80) #(N, 8, 64, 40, 40)
        row3_features = torch.sum(torch.cat((answers,row3_features), dim = 1), dim = 1) # -> (N, 10, 64, 80, 80) -> (N, 64, 80, 80)
        row3_features = self.stu_res1(row3_features) # (N, 128, 40, 40)
        row1_features = row1_features.unsqueeze(1)
        row2_features = row2_features.unsqueeze(1)

        row3_features = row3_features.unsqueeze(1) # (N, 1, 128, 40, 40)
        # print(f'row3 is : {row3_features.shape}')
        # print(f'row2 is : {row2_features.shape}')
        # print(f'row1 is : {row1_features.shape}')
        choice = torch.sum(torch.cat((row1_features, row2_features, row3_features), dim = 1), dim = 1)
        choice = self.stu_res2(choice) # (N, 256, 20, 20)
        choice = self.stu_avgpool(choice).view(-1, 256) # -> (N, 256)

        final = self.summarize(choice) # (N, 8)

        # target = torch.zeros(16, 8).cuda().scatter_(1, target.view(-1, 1), 1)
        # loss = F.binary_cross_entropy_with_logits(final, target)

        meta = self.meta_mlp(meta_target)
        meta_loss = F.binary_cross_entropy_with_logits(final, meta)

        return final,meta_loss



        # answers = self.maxpool(self.relu(self.inf_bn1(self.inf_conv1(answers.contiguous.view(-1, 160, 160).unsqueeze(1))))) # (N*8, 64, 64, 40)
        # answers = answers.view(-1, 8, 64, 40, 40)
        #
        # candidate_column = torch.index_select(input_features, 1, torch.LongTensor([2, 5])) #(N, 2, 64, 40, 40)
        # can_one = torch.cat((candidate_column, x[:, 8, :, :]), dim = 1) # (N, 3, 64, 40, 40)
        # can_two = torch.cat((candidate_column, x[:, 9, :,:]), dim = 1) # (N ,3 , 64, 40, 40)
        #student model