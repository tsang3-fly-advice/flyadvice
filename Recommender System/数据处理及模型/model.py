import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable

class Reab3p16(nn.Module):

    def __init__(self):
        super(Reab3p16, self).__init__()
        self.NUM_PANELS = 16
        self.cnn = nn.Sequential(
            nn.Conv2d(1, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.cnn_global = nn.Sequential(
            nn.Conv2d(16, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, 2),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.pre_g_fc = nn.Linear(32 * 9 ** 2, 256)
        self.pre_g_batch_norm = nn.BatchNorm1d(256)
        self.pre_g_fc2 = nn.Linear(32 * 9 ** 2, 256)
        self.pre_g_batch_norm2 = nn.BatchNorm1d(256)
        self.g = nn.Sequential(
            nn.Linear(512+512, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
            nn.Linear(1024, 256*3),
            nn.BatchNorm1d(256*3),
            nn.ReLU(),
            nn.Linear(256*3, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout()
        )
        self.g2 = nn.Sequential(
            nn.Linear(512 + 512, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
            nn.Linear(1024, 256 * 3),
            nn.BatchNorm1d(256 * 3),
            nn.ReLU(),
            nn.Linear(256 * 3, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout()
        )
        self.f = nn.Sequential(
            nn.Linear(512, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Linear(256, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(256, 1)
        )
        self.meta_fc= nn.Sequential(
            nn.Linear(512, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(256, 9)
        )
    def comp_panel_embedding(self, panel):
        batch_size = panel.shape[0]
        panel = torch.unsqueeze(panel, 1)  # (batch_size, 160, 160) -> (batch_size, 1, 160, 160)
        panel_embedding = self.cnn(panel) # (batch_size, 1, 160, 160) -> (batch_size, 32, 9, 9)
        panel_embedding = panel_embedding.view(batch_size, -1)
        panel_embedding = self.pre_g_fc(panel_embedding)
        panel_embedding = self.pre_g_batch_norm(panel_embedding)
        panel_embedding = F.relu(panel_embedding)
        return panel_embedding

    def panel_comp_obj_pairs(self, objs,batch_size):
        obj_pairses_r =torch.zeros(batch_size, 2, 256*3).cuda()
        obj_pairses_c = torch.zeros(batch_size, 2, 256 * 3).cuda()
        obj_pairses= torch.zeros(batch_size, 54, 256 * 3).cuda()
        count=0
        index=0
        for i in range(8):
            for j in range(i):
                for k in range(j):
                    if ((7-i)==0 and ((7-j)==1) and ((7-k)==2)) or((7-i)==3 and ((7-j)==4) and ((7-k)==5)):
                        obj_pairses_r[:, (7-i)//3, :] = torch.cat(
                            [torch.cat([objs[:, 7 - i, :], objs[:, 7 - j, :]], 1), objs[:, 7 - k, :]], 1)
                        count -= 1
                    elif ((7-i)==0 and ((7-j)==3) and ((7-k)==6)) or((7-i)==1 and ((7-j)==4) and ((7-k)==7)):
                        obj_pairses_c[:, 7-i, :] = torch.cat(
                            [torch.cat([objs[:, 7 - i, :], objs[:, 7 - j, :]], 1), objs[:, 7 - k, :]], 1)
                        count -= 1
                    else:
                        obj_pairses[:,count,:] = torch.cat([torch.cat([objs[:, 7 - i, :], objs[:, 7 - j, :]],1), objs[:, 7-k, :]], 1)
                    #obj_pairs = torch.cat([torch.unsqueeze( objs[:,7-i,:],1),torch.unsqueeze( objs[:,7-j,:],1)],2)
                    #obj_pairses[:,count,:] = torch.cat([obj_pairs, torch.unsqueeze(objs[:, 7-k, :], 1)], 2)
                    count+=1
        return obj_pairses,obj_pairses_c,obj_pairses_r

    def ans_comp_obj_pairs(self, ans,pan,batch_size):
        obj_pairses_r = torch.zeros(batch_size, 1, 256 * 3).cuda()
        obj_pairses_c = torch.zeros(batch_size, 1, 256 * 3).cuda()
        obj_pairses=torch.zeros(batch_size, 26, 256*3).cuda()
        count=0
        for i in range(8):
            for j in range(i):
                if (7-i)==2 and ((7-j)==5)  :
                    obj_pairs = torch.cat([pan[:, 7 - i, :], pan[:, 7 - j, :]], 1)
                    obj_pairses_c[:, 0, :] = torch.cat([obj_pairs, ans], 1)
                    count -= 1
                elif (7-i)==6 and ((7-j)==7) :
                    obj_pairs = torch.cat([pan[:, 7 - i, :], pan[:, 7 - j, :]], 1)
                    obj_pairses_r[:, 0, :] = torch.cat([obj_pairs, ans], 1)
                    count -= 1
                else:
                    obj_pairs = torch.cat([pan[:,7-i,:],pan[:,7-j,:]],1)
                    obj_pairses[:,count,:] = torch.cat([obj_pairs, ans], 1)
                count+=1
        return obj_pairses,obj_pairses_c,obj_pairses_r

    def g_functin(self,context_pairs,panel_embedding_8,num_context_pairs,batch_size):
        context_pairs = torch.cat([context_pairs, panel_embedding_8.repeat(1, num_context_pairs, 1)], 2)
        context_pairs = context_pairs.view(batch_size * num_context_pairs, 1024)
        context_g_out = self.g(context_pairs)
        context_g_out = context_g_out.view(batch_size, num_context_pairs, 512)
        context_g_out = context_g_out.sum(1)
        return context_g_out

    def g_functin2(self,context_pairs,panel_embedding_8,num_context_pairs,batch_size):
        context_pairs = torch.cat([context_pairs, panel_embedding_8.repeat(1, num_context_pairs, 1)], 2)
        context_pairs = context_pairs.view(batch_size * num_context_pairs, 1024)
        context_g_out = self.g2(context_pairs)
        context_g_out = context_g_out.view(batch_size, num_context_pairs, 512)
        context_g_out = context_g_out.sum(1)
        return context_g_out

    def forward(self, x, target, meta_target):

        x, meta_target = x.float(), meta_target.float()
        batch_size = x.shape[0]
        # Compute panel embeddings
        panel_embeddings = torch.zeros(batch_size, self.NUM_PANELS, 256).cuda()
        panel_embedding_8 = self.cnn_global(x[:, :, :, :])
        panel_embedding_8 = self.pre_g_fc2(panel_embedding_8.view(batch_size, -1))
        panel_embedding_8 = self.pre_g_batch_norm2(panel_embedding_8)
        panel_embedding_8 = F.relu(panel_embedding_8)
        panel_embedding_8 = torch.unsqueeze(panel_embedding_8, 1)
        for panel_ind in range(self.NUM_PANELS):
            panel = x[:, panel_ind, :, :]
            panel_embedding = self.comp_panel_embedding(panel)
            panel_embeddings[:, panel_ind, :] = panel_embedding
        context_embeddings = panel_embeddings[:, :int(self.NUM_PANELS/2), :] # (batch_size, 8, 256)
        answer_embeddings = panel_embeddings[:, int(self.NUM_PANELS/2):, :] # (batch_size, 8, 256)
        num_context_pairs = 56
        # Compute context pairs once to be used for each answer
        obj_pairses, obj_pairses_c, obj_pairses_r = self.panel_comp_obj_pairs(context_embeddings,batch_size)# (batch_size, 56, 256*3)
        context_g_out1= self.g_functin2(obj_pairses,panel_embedding_8,54,batch_size)
        context_g_outr = self.g_functin(obj_pairses_r, panel_embedding_8, 2, batch_size)
        context_g_outc = self.g_functin(obj_pairses_c, panel_embedding_8, 2, batch_size)
        context_g_out = context_g_out1+context_g_outc+context_g_outr
        f_out = torch.zeros(batch_size, int(self.NUM_PANELS/2)).cuda()
        f_meta=torch.zeros(batch_size, 512).cuda()
        for answer_ind in range(8):
            answer_embedding = answer_embeddings[:, answer_ind, :] # (batch_size, 256)
            context_answer_pairs,context_answer_pairs_c,context_answer_pairs_r = self.ans_comp_obj_pairs(answer_embedding,context_embeddings,batch_size)# (batch_size, 28, 512)
            context_answer_g_out1 = self.g_functin2(context_answer_pairs, panel_embedding_8, 26, batch_size)
            context_answer_g_outr = self.g_functin(context_answer_pairs_r, panel_embedding_8, 1, batch_size)
            context_answer_g_outc = self.g_functin(context_answer_pairs_c, panel_embedding_8, 1, batch_size)
            context_answer_g_out = context_answer_g_out1 + context_answer_g_outc  + context_answer_g_outr
            g_out = context_g_out + context_answer_g_out
            f_out[:, answer_ind] = self.f(g_out).squeeze()
        return F.log_softmax(f_out, dim=1)


class multipanel(nn.Module):
    def __init__(self):
        super(multipanel,self).__init__()
        self.conv_panel = nn.Sequential(nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False),
                                        nn.BatchNorm2d(64),
                                        nn.ReLU(), nn.MaxPool2d(kernel_size=3, stride=2, padding=1))

        self.resblock1 = ResBlock(64, 128, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(128)))

        self.resblock2 = ResBlock(128, 256, stride=2, downsample=nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False), nn.BatchNorm2d(256)))

        self.summary = nn.Sequential(nn.Linear(64, 64), nn.ReLU(), nn.Linear(64, 1))
        self.avgpool = nn.AdaptiveAvgPool2d((1,1))
        self.embedding = nn.Sequential(nn.Linear(256, 256), nn.ReLU())
        self.inference = nn.Softmax(dim = -1)
        self.convert = nn.Sequential(nn.Linear(64, 80), nn.ReLU())
        self.fit = nn.Sequential(nn.Linear(256, 80))
        self.bias = nn.Linear(8, 64, bias = False)


    def forward(self, x, target, meta_target):
        x = x.float()
        meta_target = meta_target.float()

        list_global = []
        list_local = []

        batch_size = x.shape[0]

        candidate = x[:, :8, :, :] # (N, 8, 160, 160)
        answer = x[:, 8:, :, :] # (N, 8, 160, 160)

        candidate = self.conv_panel(candidate.contiguous().view(-1, 160, 160).unsqueeze(1))  # -> (8N, 1, 160, 160) -> (8N, 64, 40, 40)
        answer = self.conv_panel(answer.contiguous().view(-1, 160, 160).unsqueeze(1)) # -> (8N, 64, 40, 40)

        candidate = candidate.view(-1, 8, 64, 40, 40)
        answer = answer.view(-1, 8, 64, 40, 40)

        row1 = torch.sum(candidate[:, :3, :, :], dim = 1) # (N, 8, 64, 40, 40) -> (N, 64, 40, 40)
        row2 = torch.sum(candidate[:, 3:6, :, :], dim = 1) # -> (N, 64, 40, 40)
        row3 = candidate[:, 6:8, :, :, :]

        row1 = self.resblock1(row1) # -> (N, 128, 20, 20)
        row1 = self.resblock2(row1) # -> (N, 256, 10, 10)

        row2 = self.resblock1(row2) # -> (N, 128, 20, 20)
        row2 = self.resblock2(row2) # -> (N, 256, 10, 10)

        row1_embedding = self.avgpool(row1).view(batch_size, 256) # -> (N, 256)
        row2_embedding = self.avgpool(row2).view(batch_size, 256) # -> (N, 256)

        row1_embedding = self.embedding(row1_embedding) # -> (N, 512) -> (N, 256)
        row2_embedding = self.embedding(row2_embedding) # -> (N, 512) -> (N, 256)

        row1_embedding = self.fit(row1_embedding).view(-1, 8)
        row2_embedding = self.fit(row2_embedding).view(-1, 8) # -> (10N, 8)


        row1_embedding = self.inference(row1_embedding)
        row1_embedding = self.bias(row1_embedding) # -> (10N, 64)
        row1_embedding = torch.sum(row1_embedding.view(-1, 10, 64), dim = 1) # -> (N, 64)

        row2_embedding = self.inference(row2_embedding)
        row2_embedding = self.bias(row2_embedding)
        row2_embedding = torch.sum(row2_embedding.view(-1, 10, 64), dim = 1) # -> (N, 64)


        # Global Similarity

        row3 = torch.index_select(candidate, 1, torch.LongTensor([6, 7]).cuda()) # (N, 2, 64, 40, 40)

        a = torch.zeros((batch_size, 8)).cuda()

        for i in range(8):

            candidate_one = torch.sum(torch.cat((row3, answer[:, i, :, :, :].unsqueeze(1)), dim=1), dim=1)  # -> (N, 3, 64, 40, 40) -> (N, 64, 40, 40)
            candidate_one = self.resblock1(candidate_one)  # -> (N, 128, 20, 20)
            candidate_one = self.resblock2(candidate_one)  # -> (N, 256, 10, 10)
            candidate_one = self.avgpool(candidate_one).view(batch_size, 256)  # -> (N, 256)

            candidate_one = self.embedding(candidate_one)  # -> (N, 512) -> (N, 256)
            candidate_one = self.fit(candidate_one).view(-1, 8)
            candidate_one = self.inference(candidate_one)
            candidate_one = self.bias(candidate_one)
            candidate_one = torch.sum(candidate_one.view(-1, 10, 64), dim = 1) # -> (N, 64)

            list_global.append(candidate_one)

            candidate_one = self.summary(candidate_one).squeeze() # -> (N, 1) -> N
            a[:, i] = a[:, i] + candidate_one

        global_similarity = 0.0

        for j in list_global:
            global_similarity += torch.dist(j, row1_embedding, p = 2)
            global_similarity += torch.dist(j, row2_embedding, p = 2)


        # Local Similarity

        local = torch.index_select(candidate, 1, torch.LongTensor([2, 5]).cuda())  # (N, 2, 64, 40, 40)

        photo_one = candidate[:, 2, :, :, :] # (N, 1, 64, 40, 40)
        photo_one = photo_one.squeeze(1)
        photo_one = self.resblock1(photo_one)
        photo_one = self.resblock2(photo_one)
        photo_one = self.avgpool(photo_one).view(batch_size, 256) # ->(N, 256)
        photo_one = self.embedding(photo_one) # -> (N, 256)
        photo_one = self.fit(photo_one).view(-1, 8)
        photo_one = self.inference(photo_one)
        photo_one = self.bias(photo_one)
        photo_one = torch.sum(photo_one.view(-1, 10, 64), dim=1)  # -> (N, 64)

        photo_two = candidate[:, 5, :, :, :] # (N, 1, 64, 40, 40)
        photo_two = photo_two.squeeze(1) # -> (N, 64, 40, 40)
        photo_two = self.resblock1(photo_two) # -> (N, 128, 20 ,20)
        photo_two = self.resblock2(photo_two) # -> (N, 256, 10, 10)
        photo_two = self.avgpool(photo_two).view(batch_size, 256)
        photo_two = self.embedding(photo_two) # -> (N, 256)
        photo_two = self.fit(photo_two).view(-1, 8)
        photo_two = self.inference(photo_two)
        photo_two = self.bias(photo_two)
        photo_two = torch.sum(photo_two.view(-1, 10, 64), dim=1)  # -> (N, 64)

        for result in range(8):
            daan = answer[:, result, :, :, :] # (N, 1, 64, 40, 40)
            daan = daan.squeeze(1) # -> (N, 64, 40, 40)
            daan = self.resblock1(daan) # -> (N, 128, 20, 20)
            daan = self.resblock2(daan) # -> (N, 256, 10, 10)
            daan = self.avgpool(daan).view(batch_size, 256) # -> (N, 256)
            daan = self.embedding(daan) # -> (N, 256)
            daan = self.fit(daan).view(-1, 8)
            daan = self.inference(daan)
            daan = self.bias(daan)
            daan = torch.sum(daan.view(-1, 10, 64), dim=1)  # -> (N, 64)
            list_local.append(daan)

            daan = self.summary(daan).squeeze() # -> (N, 1) -> (N)
            a[:, result] = a[:, result] + daan

        local_similarity = 0.0

        for temp in list_local:
            local_similarity += torch.dist(temp, photo_two, p = 2)
            local_similarity += torch.dist(temp, photo_one, p = 2)

        pre = F.log_softmax(a, dim = 1)

        return global_similarity, local_similarity, pre


class generalization(nn.Module):

    def __init__(self,num_attr=10, num_rule=8):
        super(generalization, self).__init__()

        self.num_attr = num_attr
        self.num_rule = num_rule

        #inference
        self.inf_conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.inf_bn1 = nn.BatchNorm2d(64)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.inf_conv_row = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.inf_bn_row = nn.BatchNorm2d(64, 64)
        self.relu = nn.ReLU(inplace=True)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.predict_rule = nn.Linear(64, self.num_attr * self.num_rule)

    #-----------------------------------------------------------------------------
        self.inference = nn.Softmax(dim=-1)
    #-----------------------------------------------------------------------------

        self.basis_bias = nn.Linear(self.num_rule, 64, bias=False)
        self.contrast1_bias_trans = nn.Sequential(nn.Linear(64,64),nn.ReLU(),nn.Dropout(0.),nn.Linear(64,64))
        self.contrast2_bias_trans = nn.Sequential(nn.Linear(64, 64), nn.ReLU(), nn.Dropout(0.), nn.Linear(64, 64))

        #perception
        self.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)

        self.conv_row = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn_row = nn.BatchNorm2d(64, 64)

        self.res1_contrast = nn.Conv2d(64+64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.res1_contrast_bn = nn.BatchNorm2d(64)
        self.res1 = ResBlock(64, 128, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False),nn.BatchNorm2d(128)))

        self.res2_contrast = nn.Conv2d(128 + 64, 128,kernel_size=3, stride=1, padding=1, bias=False)
        self.res2_contrast_bn = nn.BatchNorm2d(128)
        self.res2 = ResBlock(128, 256, stride=2, downsample=nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False),nn.BatchNorm2d(256)))

        self.mlp = nn.Sequential(nn.Linear(256,256),nn.ReLU(),nn.Linear(256,1))

        self.loss_fn = nn.BCELoss()
        self.dropout = nn.Dropout(p = 0.3)


    def forward(self, x,target, meta_target):

        x = x.float()

        # _ equals to the number of channels
        # N equals to the batch size
        N, _, H, W = x.shape

        #(N, 16, 160,160)
        #(batch_size, channel_number, height, width)

        #inference
        prior = x[:, :8, :, :]
        # newly: (N, 8, 160, 160)
        input_features = self.maxpool(self.relu(self.inf_conv1(prior.contiguous().view(-1, 160, 160).unsqueeze(1)))) #(N*8,64,40,40)
        # -> (8*N, 160, 160) -> (8*N, 1, 160, 160) -> (8*N, 64, 80, 80) -> (8*N, 64, 40, 40)

        input_features = input_features.view(-1, 8, 64, 40, 40)
        # (8*N, 64, 40, 40) -> (N, 8, 64, 40, 40)

        row1_features = torch.sum(input_features[:, 0:3, :, :, :], dim=1) #(N,64,40,40)
        row2_features = torch.sum(input_features[:, 3:6, :, :, :], dim=1)

        row_features = self.relu(self.inf_conv_row(torch.cat((row1_features, row2_features), dim=0))) #(2*N,64,40,40)
        final_row_features = row_features[:N, :, :, :] + row_features[N:, :, :, :] #(N,64,40,40)
        #(N, 64, 40, 40)
        input_features = self.avgpool(final_row_features).view(-1, 64) #(64,64)
        #(N, 64)

        predict_rules = self.predict_rule(input_features)  # N, self.num_attr * self.num_rule
        predict_rules = predict_rules.view(-1, self.num_rule) # self.num_attr*N,self.num_rule

        #----------------------------------------------------------------------------------
        predict_rules = self.inference(predict_rules)
        # inference runs with the softmax function
        #----------------------------------------------------------------------------------

        basis_bias = self.basis_bias(predict_rules)  # N * self.num_attr, 64
        basis_bias = torch.sum(basis_bias.view(-1, self.num_attr, 64), dim=1)  # N, 64

        contrast1_bias = self.contrast1_bias_trans(basis_bias) # N, 64
        contrast1_bias = contrast1_bias.view(-1, 64, 1, 1).expand(-1, -1, 40, 40) #(N,64,40,40)
        contrast2_bias = self.contrast2_bias_trans(basis_bias) #(N,64)
        contrast2_bias = contrast2_bias.view(-1, 64, 1, 1).expand(-1, -1, 20, 20) #(N,64,20,20)

        #perception
        input_features = self.maxpool(self.relu(self.conv1(x.view(-1, 160, 160).unsqueeze(1)))) #(16*N,64,40,40)
        # (N*16, 160, 160) -> (N*16, 1, 160, 160) -> (N*16, 64, 80, 80) -> (N*16, 64, 40, 40)

        input_features = input_features.view(-1, 16, 64, 40, 40) #(N,16,64,40,40)
        choices_features = input_features[:, 8:, :, :, :].unsqueeze(2) #(N,8,1,64,40,40)

        row1_features = torch.sum(input_features[:, 0:3, :, :, :], dim=1)  # N, 64, 40, 40
        row2_features = torch.sum(input_features[:, 3:6, :, :, :], dim=1)  # N, 64, 40, 40
        row3_pre = input_features[:, 6:8, :, :, :].unsqueeze(1).expand(N, 8, 2, 64, 40,40)  # N, 2, 64, 20, 20 -> N, 1, 2, 64, 20, 20 -> N, 8, 2, 64, 20, 20
        row3_features = torch.sum(torch.cat((row3_pre, choices_features), dim=2), dim=2).view(-1, 64, 40,40)  # N, 8, 3, 64, 40, 40 -> N, 8, 64, 40, 40 -> N * 8, 64, 40, 40
        row_features = self.relu(self.conv_row(torch.cat((row1_features, row2_features, row3_features), dim=0)))
        # (10*N, 64, 40, 40)

        row1 = row_features[:N, :, :, :].unsqueeze(1).unsqueeze(1).expand(N, 8, 1, 64, 40, 40)
        # (N, 8, 1, 64, 40, 40)

        row2 = row_features[N:2 * N, :, :, :].unsqueeze(1).unsqueeze(1).expand(N, 8, 1, 64, 40, 40)
        # (N, 8, 1, 64, 40, 40)

        row3 = row_features[2 * N:, :, :, :].view(-1, 8, 64, 40, 40).unsqueeze(2)
        # (8*N, 64, 40, 40) -> (N, 8, 64, 40, 40) -> (N, 8, 1, 64, 40, 40)

        final_row_features = torch.sum(torch.cat((row1, row2, row3), dim=2), dim=2)
        # -> (N, 8, 3, 64, 40, 40) -> not sure: (N, 8, 64, 40, 40)

        input_features = final_row_features
        input_features = input_features.view(-1, 64, 40, 40)
        # -> (8*N, 64, 40, 40)

        res1_in = input_features.view(-1, 8, 64, 40, 40)
        # -> (N, 8, 64, 40, 40)

        res1_contrast = self.res1_contrast(torch.cat((torch.sum(res1_in, dim=1), contrast1_bias), dim=1))
        # -> (N, 64, 40, 40) -> (N, 128, 40, 40) -> (N, 64, 20, 20)

        res1_in = res1_in - res1_contrast.unsqueeze(1)
        # (N, 8, 64, 20, 20) - (N, 1, 64, 20, 20) -> (N, 8, 64, 20, 20)

        res2_in = self.res1(res1_in.view(-1, 64, 40, 40)) #(8*N,128,20,20)
        # (8*N, 128, 20, 20)

        res2_in = res2_in.view(-1, 8, 128, 20, 20)
        # (N, 8, 128, 20, 20)

        res2_contrast =self.res2_contrast(torch.cat((torch.sum(res2_in, dim=1), contrast2_bias), dim=1)) #(N,128,20,20)
        # -> (N, 128, 20, 20) -> (N, 192, 20, 20) -> (N, 128, 20, 20)

        res2_in = res2_in - res2_contrast.unsqueeze(1) #(N,8,128,20,20)
        # -> (N, 8, 128, 20, 20)

        out = self.res2(res2_in.view(-1, 128, 20, 20)) #(8*N,256,10,10)
        # -> (8*N, 256, 20, 20)

        avgpool = self.avgpool(out)
        # -> (8*N, 256, 1, 1)

        avgpool = avgpool.view(-1, 256)
        # -> (8*N, 256)

        final = avgpool
        pre = self.mlp(final).view(-1, 8)

        # -> (8*N, 1) -> (N, 8)

        target = torch.zeros(N,8).cuda().scatter_(1,target.view(-1,1),1)
        loss = F.binary_cross_entropy_with_logits(pre,target)

        return pre,loss


class mainmodel1(nn.Module):

    def __init__(self, alpha = 1, beta = 10, num_attr=10, num_rule=8):
        super(mainmodel1, self).__init__()

        self.alpha = alpha
        self.beta = beta
        self.num_attr = num_attr
        self.num_rule = num_rule

        #inference
        self.inf_conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.inf_bn1 = nn.BatchNorm2d(64)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.inf_conv_row = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.inf_bn_row = nn.BatchNorm2d(64, 64)
        self.relu = nn.ReLU(inplace=True)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.predict_rule = nn.Linear(64, self.num_attr * self.num_rule)

    #-----------------------------------------------------------------------------
        self.inference = nn.Softmax(dim=-1)
    #-----------------------------------------------------------------------------

        self.basis_bias = nn.Linear(self.num_rule, 64, bias=False)
        self.contrast1_bias_trans = nn.Sequential(nn.Linear(64, 64),nn.PReLU(), nn.Linear(64, 64))
        self.contrast2_bias_trans = nn.Sequential(nn.Linear(64, 64), nn.PReLU(), nn.Linear(64, 64))

        #perception
        self.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)

        self.conv_row = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn_row = nn.BatchNorm2d(64, 64)

        self.res1_contrast = nn.Conv2d(64+64, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.res1_contrast_bn = nn.BatchNorm2d(64)
        self.res1 = ResBlock(64, 128, stride=2, downsample=nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False),nn.BatchNorm2d(128)))

        self.res2_contrast = nn.Conv2d(128 + 64, 128,kernel_size=3, stride=1, padding=1, bias=False)
        self.res2_contrast_bn = nn.BatchNorm2d(128)
        self.res2 = ResBlock(128, 256, stride=2, downsample=nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False),nn.BatchNorm2d(256)))

        self.mlp = nn.Sequential(nn.Linear(256,256),nn.PReLU(),nn.Linear(256,1))

        self.loss_fn = nn.BCELoss()
        self.dropout = nn.Dropout(p = 0.3)
        self.selu = selu()
        self.alpha_dropout = alpha_dropout(0.1)
        self.meta_mlp = nn.Sequential(nn.Linear(9,64), nn.PReLU(), nn.Linear(64,8))
        self.mlp_other_war_for_training = nn.Sequential(nn.Linear(9, 16), nn.PReLU(), nn.Linear(16, 32), nn.PReLU(), nn.Linear(32, 9))


    def forward(self, x,target, meta_target):

        x = x.float()
        meta_target = meta_target.float()

        # meta_target: (N,9)
        # _ equals to the number of channels
        # N equals to the batch size
        N, _, H, W = x.shape

        #(N, 16, 160,160)
        #(batch_size, channel_number, height, width)

        #inference
        prior = x[:, :8, :, :]
        # newly: (N, 8, 160, 160)
        input_features = self.maxpool(self.relu(self.inf_bn1(self.inf_conv1(prior.contiguous().view(-1, 160, 160).unsqueeze(1))))) #(N*8,64,40,40)
        # -> (8*N, 160, 160) -> (8*N, 1, 160, 160) -> (8*N, 64, 80, 80) -> (8*N, 64, 40, 40)

        input_features = input_features.view(-1, 8, 64, 40, 40)
        # (8*N, 64, 40, 40) -> (N, 8, 64, 40, 40)

        row1_features = torch.sum(input_features[:, 0:3, :, :, :], dim=1) #(N,64,40,40)
        row2_features = torch.sum(input_features[:, 3:6, :, :, :], dim=1)

        row_features = self.relu(self.inf_bn_row(self.inf_conv_row(torch.cat((row1_features, row2_features), dim=0)))) #(2*N,64,40,40)
        final_row_features = row_features[:N, :, :, :] + row_features[N:, :, :, :] #(N,64,40,40)
        #(N, 64, 40, 40)
        input_features = self.avgpool(final_row_features).view(-1, 64) #(64,64)
        #(N, 64)

        predict_rules = self.predict_rule(input_features)  # N, self.num_attr * self.num_rule
        predict_rules = predict_rules.view(-1, self.num_rule) # self.num_attr*N,self.num_rule

        #----------------------------------------------------------------------------------
        predict_rules = self.inference(predict_rules)
        # inference runs with the softmax function
        #----------------------------------------------------------------------------------

        basis_bias = self.basis_bias(predict_rules)  # N * self.num_attr, 64
        basis_bias = torch.sum(basis_bias.view(-1, self.num_attr, 64), dim=1)  # N, 64

        contrast1_bias = self.contrast1_bias_trans(basis_bias) # N, 64
        contrast1_bias = contrast1_bias.view(-1, 64, 1, 1).expand(-1, -1, 40, 40) #(N,64,40,40)
        contrast2_bias = self.contrast2_bias_trans(basis_bias) #(N,64)
        contrast2_bias = contrast2_bias.view(-1, 64, 1, 1).expand(-1, -1, 20, 20) #(N,64,20,20)

        #perception
        input_features = self.maxpool(self.relu(self.bn1(self.conv1(x.view(-1, 160, 160).unsqueeze(1))))) #(16*N,64,40,40)
        # (N*16, 160, 160) -> (N*16, 1, 160, 160) -> (N*16, 64, 80, 80) -> (N*16, 64, 40, 40)

        input_features = input_features.view(-1, 16, 64, 40, 40) #(N,16,64,40,40)
        choices_features = input_features[:, 8:, :, :, :].unsqueeze(2) #(N,8,1,64,40,40)

        row1_features = torch.sum(input_features[:, 0:3, :, :, :], dim=1)  # N, 64, 40, 40
        row2_features = torch.sum(input_features[:, 3:6, :, :, :], dim=1)  # N, 64, 40, 40
        row3_pre = input_features[:, 6:8, :, :, :].unsqueeze(1).expand(N, 8, 2, 64, 40,40)  # N, 2, 64, 20, 20 -> N, 1, 2, 64, 20, 20 -> N, 8, 2, 64, 20, 20
        row3_features = torch.sum(torch.cat((row3_pre, choices_features), dim=2), dim=2).view(-1, 64, 40,40)  # N, 8, 3, 64, 40, 40 -> N, 8, 64, 40, 40 -> N * 8, 64, 40, 40
        row_features = self.relu(self.bn_row(self.conv_row(torch.cat((row1_features, row2_features, row3_features), dim=0))))
        # (10*N, 64, 40, 40)

        row1 = row_features[:N, :, :, :].unsqueeze(1).unsqueeze(1).expand(N, 8, 1, 64, 40, 40)
        # (N, 8, 1, 64, 40, 40)

        row2 = row_features[N:2 * N, :, :, :].unsqueeze(1).unsqueeze(1).expand(N, 8, 1, 64, 40, 40)
        # (N, 8, 1, 64, 40, 40)

        row3 = row_features[2 * N:, :, :, :].view(-1, 8, 64, 40, 40).unsqueeze(2)
        # (8*N, 64, 40, 40) -> (N, 8, 64, 40, 40) -> (N, 8, 1, 64, 40, 40)

        final_row_features = torch.sum(torch.cat((row1, row2, row3), dim=2), dim=2)
        # -> (N, 8, 3, 64, 40, 40) -> not sure: (N, 8, 64, 40, 40)

        input_features = final_row_features
        input_features = input_features.view(-1, 64, 40, 40)
        # -> (8*N, 64, 40, 40)

        res1_in = input_features.view(-1, 8, 64, 40, 40)
        # -> (N, 8, 64, 40, 40)

        res1_contrast = self.res1_contrast_bn(self.res1_contrast(torch.cat((torch.sum(res1_in, dim=1), contrast1_bias), dim=1)))
        # -> (N, 64, 40, 40) -> (N, 128, 40, 40) -> (N, 64, 20, 20)

        res1_in = res1_in - res1_contrast.unsqueeze(1)
        # (N, 8, 64, 20, 20) - (N, 1, 64, 20, 20) -> (N, 8, 64, 20, 20)

        res2_in = self.res1(res1_in.view(-1, 64, 40, 40)) #(8*N,128,20,20)
        # (8*N, 128, 20, 20)

        res2_in = res2_in.view(-1, 8, 128, 20, 20)
        # (N, 8, 128, 20, 20)

        res2_contrast = self.res2_contrast_bn(self.res2_contrast(torch.cat((torch.sum(res2_in, dim=1), contrast2_bias), dim=1))) #(N,128,20,20)
        # -> (N, 128, 20, 20) -> (N, 192, 20, 20) -> (N, 128, 20, 20)

        res2_in = res2_in - res2_contrast.unsqueeze(1) #(N,8,128,20,20)
        # -> (N, 8, 128, 20, 20)

        out = self.res2(res2_in.view(-1, 128, 20, 20)) #(8*N,256,10,10)
        # -> (8*N, 256, 20, 20)

        avgpool = self.avgpool(out)
        # -> (8*N, 256, 1, 1)

        avgpool = avgpool.view(-1, 256)
        # -> (8*N, 256)

        final = avgpool
        pre = self.mlp(final).view(-1, 8)

       # -> (8*N, 1) -> (N, 8)

        #(N,9)
        meta = self.mlp_other_war_for_training(meta_target)
        meta_loss = F.cross_entropy(meta,meta_target.argmax(dim = -1).long())

        target = torch.zeros(N,8).cuda().scatter_(1,target.view(-1,1),1)
        loss = F.cross_entropy(pre, target.argmax(dim = -1).long())

        return pre, loss, meta_loss


class ResBlock(nn.Module):
    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(ResBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channel, out_channel, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channel)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_channel, out_channel,  kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(out_channel)
        self.downsample = downsample

    def forward(self, x):

        out = self.relu(self.bn1(self.conv1(x)))
        out = self.relu(self.downsample(x) + self.bn2(self.conv2(out)))

        return out


class selu(nn.Module):
    def __init__(self):
        super(selu, self).__init__()
        self.alpha = 1.6732632423543772848170429916717
        self.scale = 1.0507009873554804934193349852946

    def forward(self, x):
        temp1 = self.scale * F.relu(x)
        temp2 = self.scale * self.alpha * (F.elu(-1 * F.relu(-1 * x)))
        return temp1 + temp2


class alpha_dropout(nn.Module):
    def __init__(self, p=0.1, alpha=-1.7580993408473766, fixedPointMean=0, fixedPointVar=1):
        super(alpha_dropout, self).__init__()
        keep_prob = 1 - p
        self.a = np.sqrt(
            fixedPointVar / (keep_prob * ((1 - keep_prob) * pow(alpha - fixedPointMean, 2) + fixedPointVar)))
        self.b = fixedPointMean - self.a * (keep_prob * fixedPointMean + (1 - keep_prob) * alpha)
        self.alpha = alpha
        self.keep_prob = 1 - p
        self.drop_prob = p

    def forward(self, x):
        if self.keep_prob == 1 or not self.training:
            # print("testing mode, direct return")
            return x
        else:
            random_tensor = self.keep_prob + torch.rand(x.size())

            binary_tensor = Variable(torch.floor(random_tensor))

            if torch.cuda.is_available():
                binary_tensor = binary_tensor.cuda()

            x = x.mul(binary_tensor)
            ret = x + self.alpha * (1 - binary_tensor)
            ret.mul_(self.a).add_(self.b)
            return ret